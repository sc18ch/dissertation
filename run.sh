#!/bin/sh
echo ">>> Running program..."
JAVAFX_PATH="./java/lib/javafx-sdk-17.0.2/lib"
JAVAFX_MODULES="javafx.base,javafx.controls,javafx.fxml,javafx.graphics,javafx.media,javafx.swing,javafx.web"
JSON_PATH="./java/lib"
JSON_MODULE="org.json"

java --module-path ${JAVAFX_PATH}:${JSON_PATH} --add-modules=${JAVAFX_MODULES},${JSON_MODULE} -cp "./java/bin/:./java/assets/" main.Main
