#!/bin/sh
read -p "Enter model (.mzn) file name: " MODEL_NAME
read -p "Enter data (.dzn) file name: " DATA_NAME
read -p "Enter log (.log) file name: " LOG_NAME
minizinc --solver gurobi --output-objective --unsatorunbnd-msg --output-time --output-mode json -o logs/$LOG_NAME.log models/knapsack/$MODEL_NAME.mzn instances/knapsack/$DATA_NAME.dzn
if [ $? -eq 0 ]; then
    echo ">>> SOLVE COMPLETE! - Log file saved to logs/${LOG_NAME}.log"
else
    echo ">>> SOLVE FAILED"
fi
