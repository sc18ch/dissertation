from random import randint
import math

n = int(input("Enter the number of boxes n: "))
binSizeX = int(input("Enter a fixed size in dimension x for bin: "))
binSizeY = int(input("Enter a fixed size in dimension y for bin: "))
binSizeZ = int(input("Enter a fixed size in dimension z for bin: "))
rand = input("Random sizes (y/n)? ")

boxSizes = []

if rand.upper() == "Y":
	lowerBound = int(input("Enter a lower bound for random box sizes: "))
	upperBound = int(input("Enter an upper bound for random box sizes: "))
	for i in range(n):
		sizes = []
		for _ in range(3):sizes.append(round(randint(lowerBound, upperBound), -1))
		boxSizes.append(sizes)
else:
	sizes = []
	sizes.append(int(input("Enter a fixed size for boxes in dimension x: ")))
	sizes.append(int(input("Enter a fixed size for boxes in dimension y: ")))
	sizes.append(int(input("Enter a fixed size for boxes in dimension z: ")))
	for i in range(n):boxSizes.append(sizes)

fileName = input("Please enter an instance file name: ")

with open(fileName + ".dzn", 'w') as f:
	output = "n = "
	output += str(n) + ";\nitemSizes = [|"
	for i in range(n):
		output += " " + str(boxSizes[i][0]) + ", " + str(boxSizes[i][1]) + ", " + str(boxSizes[i][2]) + ",|"
	output += "];\nbin = [" + str(binSizeX) + ", " + str(binSizeY) + ", " + str(binSizeZ) + "];"
	f.write(output)
	f.close()
