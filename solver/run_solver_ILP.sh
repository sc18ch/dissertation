#!/bin/sh
MODEL_NAME="knapsack/knapsack_ILP_3D.mzn"
DATA_NAME="knapsack/random_20_boxes_3D.dzn"
LOG_NAME="run_solver_auto_output_ILP.log"
minizinc --solver gurobi --output-objective --unsatorunbnd-msg --output-time --output-mode json -o logs/$LOG_NAME models/$MODEL_NAME instances/$DATA_NAME
if [ $? -eq 0 ]; then
    echo ">>> SOLVE COMPLETE! - Log file saved to logs/${LOG_NAME}"
else
    echo ">>> SOLVE FAILED"
fi
