% Problem definition for an ILP model to solve a 3D-KP (no rotations)
% By Conna Haw-Wells

enum dimensions = {x, y, z};

% ---- Inputs ----

% Item input
int: n; % no. of items
set of int: numberOfItems = 1..n;
array[numberOfItems, dimensions] of int: itemSizes; % Item sizes
set of int: numberOfIntermediary = 1..6;

% Bin input
array[dimensions] of int: bin;

% Large constant M
int: M = greatestDimension(bin) + 1;

% ---- Decision Variables ----

array[numberOfItems] of var 0..1: placed; % Array indicating 1 if placed in bin and 0 otherwise
array[numberOfItems, dimensions] of var 0..M: itemCoordinates; % Array containing item position

array[numberOfIntermediary, numberOfItems, numberOfItems] of var 0..1: intermediaryX;
array[numberOfIntermediary, numberOfItems, numberOfItems] of var 0..1: intermediaryY;
array[numberOfIntermediary, numberOfItems, numberOfItems] of var 0..1: intermediaryZ;
array[numberOfItems, numberOfItems] of var 0..1: alpha;
array[numberOfItems, numberOfItems] of var 0..1: beta;
array[numberOfItems, numberOfItems] of var 0..1: gamma;

% ---- Functions ----

% Function to determine biggest dimension in bin
function int: greatestDimension(array[dimensions] of int: cube) =
    if cube[x] > cube[y] then (if cube[x] > cube[z] then cube[x] else cube[z] endif) else (if cube[y] > cube[z] then cube[y] else cube[z] endif) endif;
    
function int: volume(int: item) = itemSizes[item, x] * itemSizes[item, y] * itemSizes[item, z];

% ---- Objective ----

% Maximise the filled space inside the bin
solve maximize sum(item in numberOfItems)(volume(item) * placed[item]);

% ---- Constraints ----

% Constraint to keep items inside bin if placed
constraint forall(item in numberOfItems)(
    itemCoordinates[item, x] + itemSizes[item, x] - M * (1 - placed[item]) <= bin[x] /\
    itemCoordinates[item, y] + itemSizes[item, y] - M * (1 - placed[item]) <= bin[y] /\
    itemCoordinates[item, z] + itemSizes[item, z] - M * (1 - placed[item]) <= bin[z]
);

% Constraint to stop items from overlapping
constraint forall(item1 in numberOfItems)(
    forall(item2 in numberOfItems where item1 != item2)(
    
        % Overlap in x constraints
        itemCoordinates[item2, x] - itemCoordinates[item1, x] + 1 <= M * intermediaryX[1, item1, item2] /\
        itemCoordinates[item1, x] - itemCoordinates[item2, x] + 1 <= M * intermediaryX[2, item1, item2] /\
        itemCoordinates[item1, x] + itemSizes[item1, x] - itemCoordinates[item2, x] <= M * intermediaryX[3, item1, item2] /\
        itemCoordinates[item2, x] + itemSizes[item2, x] - itemCoordinates[item1, x] <= M * intermediaryX[4, item1, item2] /\
        intermediaryX[1, item1, item2] + intermediaryX[3, item1, item2] <= 1 + intermediaryX[5, item1, item2] /\
        intermediaryX[2, item1, item2] + intermediaryX[4, item1, item2] <= 1 + intermediaryX[6, item1, item2] /\
        intermediaryX[5, item1, item2] + intermediaryX[6, item1, item2] <= 2 * alpha[item1, item2] /\
        
        % Overlap in y constraints
        itemCoordinates[item2, y] - itemCoordinates[item1, y] + 1 <= M * intermediaryY[1, item1, item2] /\
        itemCoordinates[item1, y] - itemCoordinates[item2, y] + 1 <= M * intermediaryY[2, item1, item2] /\
        itemCoordinates[item1, y] + itemSizes[item1, y] - itemCoordinates[item2, y] <= M * intermediaryY[3, item1, item2] /\
        itemCoordinates[item2, y] + itemSizes[item2, y] - itemCoordinates[item1, y] <= M * intermediaryY[4, item1, item2] /\
        intermediaryY[1, item1, item2] + intermediaryY[3, item1, item2] <= 1 + intermediaryY[5, item1, item2] /\
        intermediaryY[2, item1, item2] + intermediaryY[4, item1, item2] <= 1 + intermediaryY[6, item1, item2] /\
        intermediaryY[5, item1, item2] + intermediaryY[6, item1, item2] <= 2 * beta[item1, item2] /\
        
        % Overlap in z constraints
        itemCoordinates[item2, z] - itemCoordinates[item1, z] + 1 <= M * intermediaryZ[1, item1, item2] /\
        itemCoordinates[item1, z] - itemCoordinates[item2, z] + 1 <= M * intermediaryZ[2, item1, item2] /\
        itemCoordinates[item1, z] + itemSizes[item1, z] - itemCoordinates[item2, z] <= M * intermediaryZ[3, item1, item2] /\
        itemCoordinates[item2, z] + itemSizes[item2, z] - itemCoordinates[item1, z] <= M * intermediaryZ[4, item1, item2] /\
        intermediaryZ[1, item1, item2] + intermediaryZ[3, item1, item2] <= 1 + intermediaryZ[5, item1, item2] /\
        intermediaryZ[2, item1, item2] + intermediaryZ[4, item1, item2] <= 1 + intermediaryZ[6, item1, item2] /\
        intermediaryZ[5, item1, item2] + intermediaryZ[6, item1, item2] <= 2 * gamma[item1, item2] /\
        
        % Constraint for overlap in all 3 dimensions
        alpha[item1, item2] + beta[item1, item2] + gamma[item1, item2] <= 4 - placed[item1] - placed[item2]
        
    )
);

% ---- Output ----
output [if dimension == 1 /\ item == 1 then "\n-- ITEMS -- \n" else "" endif ++ if dimension == 1 then "    Item " ++ format(item) ++ " | Placed = " ++ if format(placed[item]) == "1" then "TRUE " else "FALSE" endif ++ " -->  " else " " endif ++ format(dimension) ++ ":" ++ show_int(4, itemCoordinates[item, dimension]) ++
        if dimension == 3 then "\n" else " " endif |
        item in numberOfItems, dimension in dimensions
];
% Alpha, Beta and Gamma outputs for debug
/*
output [if item1 == 1 /\ item2 == 1 then "\nALPHA:\n" else "" endif ++ if item2 == 1 then "Item " ++ format(item1) ++ " -->  " else " " endif ++ "Item " ++ format(item2) ++ ": " ++ format(alpha[item1, item2]) ++ if item2 == n then "\n" else " " endif |
        item1 in numberOfItems, item2 in numberOfItems
];

output [if item1 == 1 /\ item2 == 1 then "\nBETA:\n" else "" endif ++ if item2 == 1 then "Item " ++ format(item1) ++ " -->  " else " " endif ++ "Item " ++ format(item2) ++ ": " ++ format(beta[item1, item2]) ++ if item2 == n then "\n" else " " endif |
        item1 in numberOfItems, item2 in numberOfItems
];

output [if item1 == 1 /\ item2 == 1 then "\nGAMMA:\n" else "" endif ++ if item2 == 1 then "Item " ++ format(item1) ++ " -->  " else " " endif ++ "Item " ++ format(item2) ++ ": " ++ format(gamma[item1, item2]) ++ if item2 == n then "\n" else " " endif |
        item1 in numberOfItems, item2 in numberOfItems
];
*/
output ["\nBin Usage: " ++ show_float(0, 2, ((sum(item in numberOfItems)(volume(item) * placed[item])) / (bin[x] * bin[y] * bin[z])) * 100) ++ "%"];
output ["\nBox Usage: " ++ show_float(0, 2, ((sum(item in numberOfItems)(placed[item])) / (n)) * 100) ++ "%, " ++ show_int(0, ((sum(item in numberOfItems)(placed[item])))) ++ "/" ++ show_int(0, n) ++ " boxes placed"];