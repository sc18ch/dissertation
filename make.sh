#!/bin/sh
echo ">>> Building project..."
JAVAFX_PATH="./java/lib/javafx-sdk-17.0.2/lib"
JAVAFX_MODULES="javafx.base,javafx.controls,javafx.fxml,javafx.graphics,javafx.media,javafx.swing,javafx.web"
JSON_PATH="./java/lib"
JSON_MODULE="org.json"

javac --module-path ${JAVAFX_PATH}:${JSON_PATH} --add-modules=${JAVAFX_MODULES},${JSON_MODULE} -sourcepath ./java/src -d ./java/bin ./java/src/**/*.java
if [ $? -eq 0 ]; then
    echo ">>> BUILD COMPLETE!"
else
    echo ">>> BUILD FAILED!"
fi
