\justifying

\chapter{Introduction and Background Research}

% You can cite chapters by using '\ref{chapter1}', where the label must
% match that given in the 'label' command, as on the next line.
\label{chapter1}

% Sections and sub-sections can be declared using \section and \subsection.
% There is also a \subsubsection, but consider carefully if you really need
% so many layers of section structure.
\section{Introduction} \label{sec:Introduction}

%<A brief introduction suitable for a non-specialist, {\em i.e.} without using technical terms or jargon, as far as possible. This may be similar/the same as that in the 'Outline and Plan' document. The remainder of this chapter will normally cover everything to be assessed under the `Background Research` criterion in the mark scheme.>
\subsection{Optimisation problems}
An \textit{optimisation problem} is the task of finding the most optimal solution amid all possible solutions. This is done by first defining an objective function $f(x)$, whose value is desired to be minimised or maximised in the optimal solution $x$. A series of constraints are then imposed on $x$, to which a feasible solution must comply with. Depending on a minimisation or maximisation problem, a solution $x$ is considered optimal if there exists no other $x$ such that $f(x)$ is more minimal or maximal. In practice, these problems appear in many real world contexts such as business and manufacturing; for example companies wanting to minimise production costs or maximise revenue.

\subsection{The knapsack problem} \label{sssec:definitionKP}
The \textit{knapsack problem} (KP) is a classic optimisation problem which consists of a bin of fixed size. A finite set of items must then be 'packed' into this bin, usually with the goal of minimising the wasted space inside the bin. The most common version of the problem, the \textit{0-1 knapsack problem}, can be seen below. This formulation is based upon the one present in the seminal book by Martello \& Toth \cite{martello1990knapsack} in 1990, who are considered pioneers in the KP area. Note that the bin can also be referred to as the container in a KP:
\begin{center}
	\def\arraystretch{2}
	\begin{tabular}{cc}
		\textbf{Maximise} & $\mathlarger{\mathlarger{\sum}_{i=1}^{n}}v_ix_i$ \\
		\textbf{Subject to}	& $\mathlarger{\mathlarger{\sum}_{i=1}^{n}}w_ix_i \leq c$, \\
		& $x_i \in \{0, 1\}, i = 1,...,n$ \\
	\end{tabular}
\end{center}

This formulation defines a container $c$ and $n$ items, denoted by $i$. Of item $i$, constant $v_i$ is the value and $w_i$ is the weight of that item. Finally, $x_i$ is an indicator variable where $x_i = 1$ shows item $i$ is placed in the container, and $x_i = 0$ shows $i$ is not placed.

Inferring from the formulation, an optimal solution is the solution with the largest total value of placed items in the container, which also adheres to the constraint that the total weight of placed items cannot exceed the maximum weight of the container. KP derives it's name from the real life problem in which a person is faced with loading a fixed-size knapsack with items, making sure to maximise the total value of items placed in the knapsack, without overloading the maximum capacity of the knapsack.

% Picture figure for KP
\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\draw (0,4) -- (0,0) -- (2,0) -- (2,4);
		\draw[thick,<->,dashed] (-0.3,0) -- (-0.3,4) node[midway, anchor=east] {$c$};
		\draw[thick,<->,dashed] (2.3,0) -- (2.3,1) node[midway, anchor=west] {$w_1$};
		\filldraw[cyan!70!white, draw=black] (0,0) rectangle node[midway, black]{$i=1$} (2,1);
		\filldraw[yellow!70!white, draw=black] (0,1) rectangle node[midway, black]{$i=2$} (2,2.5);
		\filldraw[orange!70!white, draw=black] (0,2.5) rectangle node[midway, black]{$i=3$} (2,3.5);
		\filldraw[pink!70!white, draw=black] (4,0) rectangle node[midway, black]{$i=4$} (6,1.5);
	\end{tikzpicture}
	\caption{An example optimal solution where $x_1, x_2, x_3 = 1$, and $x_4 = 0$. This KP instance has four items and container $c$, where $v_1, v_2, v_3 = 3$, and $v_4 = 1$}
\end{figure}
\FloatBarrier

There are two other types of KP which are not as prominent in research and practice as the 0-1 KP: the \textit{bounded knapsack problem} (BKP) and \textit{unbounded knapsack problem} (UKP). The BKP removes the restriction that items can only be placed once, and replaces this with a non-negative integer constant which defines the maximum number of copies of a given item that are allowed to be placed inside the knapsack. Naturally, UKP is a KP in which there is no restriction to the amount of copies of an item that can be placed. Similar to other literature, when referring to KPs, this report will be considering the 0-1 KP and its variants.

\subsection{Notable problem variants} \label{NotableVariants}
All problem variants of the KP are built upon or formulated similarly to the definition in Section \ref{sssec:definitionKP}; more constraints or more inputs are added to define what a solution is for a particular variant. Some notable variants are discussed in this section.

\subsubsection{Bin Packing Problem (BPP)}
Instead of a single bin, the BPP defines a finite set of bins, each of a fixed capacity, in which items are to be 'packed'. Note that this problem is considered separate to the KP, yet their definitions closely align; thus the BPP can be interpreted as a variation of a KP \cite{martello1990knapsack}. The item weights are generally referred to as item sizes in this problem, however they can also be interpreted as the same.

\subsubsection{Two-dimensional geometric Knapsack Problem (2D-KP)}

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}[scale=0.85]
		\draw (0,0) -- (5,0) -- (5,4.5) -- (0,4.5) -- (0,0);
		\draw[thick,<->,dashed] (0,-0.3) -- (5,-0.3) node [midway, anchor=north] {$x$};
		\draw[thick,<->,dashed] (-0.3,0) -- (-0.3,4.5) node[midway, anchor=east] {$y$};
		\filldraw[pink!70!white, draw=black] (0,0) rectangle (1.5,2.5);
		\filldraw[orange!70!white, draw=black] (2.5,1.7) rectangle (4,4);
		\filldraw[cyan!70!white, draw=black] (2.5,0) rectangle (5,1.7);
		\filldraw[yellow!70!white, draw=black] (1.5,0) rectangle (2.5,3);
	\end{tikzpicture}
	\caption{An example solution of a 2D-KP instance with four items and a container $(x, y)$}
\end{figure}
\FloatBarrier

2D-KP focuses on a KP instance in the geometric space, where items with sizes in dimensions $x$ and $y$ are placed orthogonally inside the container. Constraints are in place so that items do not overlap with each other, and do not exist outside of the container. The value and weight of an item is the area it occupies within the container.

\subsubsection{Three-dimensional geometric Knapsack Problem (3D-KP)}

% Picture figure for 3D-KP
\begin{figure}[!h]
	\centering
	\begin{tikzpicture}[scale=0.9]
		\newcommand{\xA}{2}
		\newcommand{\yA}{2}
		\newcommand{\zA}{2}
		\newcommand{\xB}{3}
		\newcommand{\yB}{3}
		\newcommand{\zB}{3}
		\newcommand{\xC}{3}
		\newcommand{\yC}{1}
		\newcommand{\zC}{1}
		
		\coordinate (A1) at (0,0,-1);
		\coordinate (A2) at (\xA,0,-1);
		\coordinate (A3) at (\xA,\yA,-1);
		\coordinate (A4) at (0,\yA,-1);
		\coordinate (A5) at (0,0,-\zA - 1);
		\coordinate (A6) at (\xA,0,-\zA - 1);
		\coordinate (A7) at (\xA,\yA,-\zA - 1);
		\coordinate (A8) at (0,\yA,-\zA - 1);
		
		\coordinate (B1) at (\xA,0,-\zA);
		\coordinate (B2) at (\xA + \xB,0,-\zA);
		\coordinate (B3) at (\xA + \xB,\yB, - \zA);
		\coordinate (B4) at (\xA,\yB,-\zA);
		\coordinate (B5) at (\xA,0,-\zB - \zA);
		\coordinate (B6) at (\xA + \xB,0,-\zB - \zA);
		\coordinate (B7) at (\xA + \xB,\yB,-\zB - \zA);
		\coordinate (B8) at (\xA,\yB,-\zB - \zA);
		
		\coordinate (C1) at (\xA,0,0);
		\coordinate (C2) at (\xA + \xC,0,0);
		\coordinate (C3) at (\xA + \xC,\yC,0);
		\coordinate (C4) at (\xA,\yC,0);
		\coordinate (C5) at (\xA,0,-\zC);
		\coordinate (C6) at (\xA + \xC,0,-\zC);
		\coordinate (C7) at (\xA + \xC,\yC,-\zC);
		\coordinate (C8) at (\xA,\yC,-\zC);
		
		\draw (0,0,0) -- (0,0,-5) -- (0,3,-5) -- (0,3,0) -- cycle;
		\draw (0,0,-5) -- (5,0,-5) -- (5,3,-5) -- (0,3,-5) -- cycle;
		
		\draw[black, fill=blue!80] (A1) -- (A2) -- (A3) -- (A4) -- cycle;
		\draw[black, fill=red!80] (A2) -- (A6) -- (A7) -- (A3) -- cycle;
		\draw[black, fill=yellow!80] (A4) -- (A3) -- (A7) -- (A8) -- cycle;
		
		\draw[black, fill=cyan!80] (B1) -- (B2) -- (B3) -- (B4) -- cycle;
		\draw[black, fill=pink!80] (B2) -- (B6) -- (B7) -- (B3) -- cycle;
		\draw[black, fill=green!80] (B4) -- (B3) -- (B7) -- (B8) -- cycle;
		
		\draw[black, fill=orange!80] (C1) -- (C2) -- (C3) -- (C4) -- cycle;
		\draw[black, fill=darkgray!80] (C2) -- (C6) -- (C7) -- (C3) -- cycle;
		\draw[black, fill=lime!80] (C4) -- (C3) -- (C7) -- (C8) -- cycle;
		
		\draw (0,0,0) -- (5,0,0) -- (5,3,0) -- (0,3,0) -- cycle;
		\draw (5,0,0) -- (5,0,-5) -- (5,3,-5) -- (5,3,0) -- cycle;
		
		\draw[thick,<->,dashed] (0,0,0.5) -- (5,0,0.5) node [midway, anchor=north] {$x$};
		\draw[thick,<->,dashed] (-0.3,0,0) -- (-0.3,3,0) node [midway, anchor=east] {$y$};
		\draw[thick,<->,dashed] (5.3,0,0) -- (5.3,0,-5) node [midway, anchor=north west] {$z$};
		\filldraw[fill=red] (0,0,0) circle (3pt);

	\end{tikzpicture}
	\caption{An example solution of a 3D-KP instance with three items and a container $(x, y, z)$. The red dot indicates the left-bottom-front corner, respective to $x$, $y$ and $z$}
\end{figure}
The 3D-KP is exactly the same as the 2D-KP, yet it applies the KP in three geometric dimensions instead of two. In the 3D-KP and the similar 3D-BPP (three-dimensional geometric bin packing problem), items are sometimes referred to as 'boxes' \cite{silva2019} or 'cartons' \cite{chen1995}. This is likely due to the fact that items are cuboid in shape. The 3D-KP as an optimisation problem is what this paper will focus upon, specifically the orthogonal variant (i.e. each edge of a box must be parallel to the $x, y$ or $z$ axis).

% Must provide evidence of a literature review. Use sections
% and subsections as they make sense for your project.
\section{Background Research} \label{backgroundResearch}
KPs in general, as an area of combinatorial optimization, have a diverse and active domain of literature available. The recent survey by Cacchiani et al. \cite{cacchiani2022knapsackpart1}\cite{cacchiani2022knapsackpart2} (co-authored by Martello, who co-wrote the seminal 1990 book on KPs \cite{martello1990knapsack}) highlights new research into a diverse range of KP variants. Whilst some variants have a multitude of research available (e.g. the 2D-KP), others, specifically the 3D-KP and higher dimensional KPs, have a sparse amount of literature available \cite{cacchiani2022knapsackpart2}. Therefore there are limited resources available for investigations into the 3D-KP.

\subsection{Computational complexity theory} \label{complexityTheoryResearch}
\textit{Computational complexity theory} \cite{theoryOfNPCompletenessTextbook}\cite{computationalComplexityTextbook}\cite{papadimitriou1994computational} is used to classify computational problems, with harder problems requiring more resources (such as time or computation power) in order to achieve a solution. It is designed to be applied mainly to decision problems; problems in which all instances either belong to the set of \textit{yes-instances} or the set of \textit{no-instances} \cite{theoryOfNPCompletenessTextbook}. \textbf{a)} "Given a number $x$, is $x$ prime?" or \textbf{b)} "Given a graph, is there a path that visits each node exactly once?" \cite{decisionProblemsLectureNotes} are both examples of a decision problem, since any given instance can only either be part of the set of \textit{yes-instances} or \textit{no-instances}. An algorithm is said to \textit{solve} a decision problem if it can classify any given instance into the set of \textit{yes-instances} or \textit{no-instances}.

The most relevant complexity classes for this paper, when discussing computational complexity theory, are $\mathbb{P}$, $\mathbb{NP}$, $\mathbb{NP}$-complete and $\mathbb{NP}$-hard. A decision problem is classified as the complexity class:
\begin{itemize}
	\setlength\itemsep{0em}
	\item $\mathbb{P}$ if there exists a deterministic polynomial time algorithm that solves it. The prime number example \textbf{a)} above is a $\mathbb{P}$ problem \cite{agrawal2004primes}.
	\item $\mathbb{NP}$ if there exists a non-deterministic polynomial time algorithm that solves it, and if, for a single instance, it is possible to verify whether that instance is part of the set of \textit{yes-instances} using a deterministic polynomial time algorithm. Example \textbf{b)} above, the Hamiltonian Path decision problem, is an $\mathbb{NP}$ problem.
	\begin{itemize}
		\item Within $\mathbb{NP}$, there are also $\mathbb{NP}$-complete problems. Problems are defined as $\mathbb{NP}$-complete when there exists a polynomial time reduction \cite{haikoComplexityLectureNotes} to it from any other $\mathbb{NP}$ problem.
	\end{itemize}
	\item $\mathbb{NP}$-hard if there is an $\mathbb{NP}$-complete problem of which there exists a polynomial time reduction from the $\mathbb{NP}$-complete problem to the $\mathbb{NP}$-hard problem \cite{theoryOfNPCompletenessTextbook}. $\mathbb{NP}$-hard problems do not have to be in $\mathbb{NP}$, and they do not have to be decision problems. All $\mathbb{NP}$-hard problems which are in $\mathbb{NP}$ are $\mathbb{NP}$-complete.
\end{itemize} 

Decision problems in $\mathbb{P}$ are a subset of $\mathbb{NP}$, although it is still unknown whether this is a proper subset. The question of "Does $\mathbb{P}=\mathbb{NP}$?" is one of the six unsolved Millennium Problems defined by Clay Mathematics Institute \cite{milleniumProblems}, however there is a widespread conjecture that $\mathbb{P}\neq\mathbb{NP}$ \cite{theoryOfNPCompletenessTextbook}. Appendix \ref{complexityTheoryVennDiagram} displays a Venn diagram which helps demonstrate the relationship between these complexity classes.

To relate this knowledge back to KPs, it is worth noting the difference of the decision problem from the optimisation problem \cite{plusMathsKPArticle}, as seen in Figure \ref{figure:DecisionOptimisationDef}.

\vspace{1.2em}

\begin{figure}[!h]
	\centering
	\begin{tabular}{p{0.28\linewidth}p{0.6\linewidth}}
		\textbf{Decision Problem} & Given a value threshold $V$ and a knapsack, is there a solution that satisfies the weight constraint of the knapsack and exceeds value $V$?  \\
		\textbf{Optimisation Problem}	& Given a knapsack, what is the solution with greatest value that satisfies the weight constraint of the knapsack? \\
	\end{tabular}
	\caption{The KP defined as a decision problem, and the KP defined as an optimisation problem}
	\label{figure:DecisionOptimisationDef}
\end{figure}

\vspace{0.2em}

It's possible for beginners of computational complexity to be confused when classifying optimisation problems \cite{confusionOfNPOptimisationProblems}. By definition of $\mathbb{NP}$, only decision problems can be classified as $\mathbb{NP}$. Thus, whilst the decision version of the KP is $\mathbb{NP}$-complete, the optimisation problem is actually $\mathbb{NP}$-hard and not in $\mathbb{NP}$ \cite{martello1990knapsack}\cite{IPLectureNotesBerkeley}. Any optimisation problem can be converted into a corresponding decision problem by providing a cost boundary $\omega$ and asking whether the value of a solution exceeds or is less than $\omega$ \cite{leung2004handbook}, similar to $V$ in Figure \ref{figure:DecisionOptimisationDef}.

It is known that the decision version of the KP is $\mathbb{NP}$-complete since there exists polynomial time reductions from the Partition problem \cite{cornellLectureReductionToKP} and Subset Sum problem \cite{mcgillUniversityAssignmentReductionToKP} to the KP problem. Beginners may propose that both the decision version and optimisation problem of the 0-1 KP is in $\mathbb{P}$ as there exists a Dynamic Programming solution which solves the optimisation problem in $O(nV)$ time \cite{leung2004handbook}. This, however, is labelled as a \textit{pseudo-polynomial} algorithm, since it can solve in polynomial time using only a unary encoding scheme. The decision version of the KP is therefore classed as $\mathbb{NP}$-hard in the \textit{ordinary} sense \cite{leung2004handbook}.

Since the KP is $\mathbb{NP}$-hard, the problem is not solvable in polynomial time, and therefore requires the use of methods which may not be guaranteed to find the optimal solution in a reasonable time, when solving for large inputs. Generally throughout the literature, when referring to the KP, it is usually the optimisation version of the 0-1 KP that is being considered \cite{martello1990knapsack}\cite{kellerer2004knapsack}.

\subsection{Solving approaches} \label{sssec:solvingApproaches}
Drawing attention back to the 3D-KP, the focus of this paper, there is scarce literature surrounding methods of solving it and related variants.

\subsubsection{Heuristic Algorithms}
There is a trade-off between optimality and computation resources when solving non-$\mathbb{P}$ problems in polynomial time. Heuristic algorithms are often polynomial time algorithms which can result in a solution which is on average 'good', but not always optimal. Whether the solution outputs are considered on average 'good' is subject to the problem and context it is being solved in. An example of this for the 3D-KP can be seen in Baldi et al. \cite{heuristicAlgorithm3DKP}. Heuristics are sometimes used to provide a baseline solution to be passed to an optimisation algorithm, which optimises from the baseline further.

\subsubsection{Approximation Algorithms}
Approximation algorithms are heuristic algorithms which offer a provable guarantee (a known error bound) on how close a computed solution is to optimal. Since it is widely believed $\mathbb{P}\neq\mathbb{NP}$, there is need for polynomial algorithms to approximate a near optimal solution for problems not in $\mathbb{P}$ for situations where efficiency of computation time, or other resources, is vital. An example of approximation algorithms for the 3D-KP can be seen in Diedrich et al. in 2007 \cite{approximationAlgorithms3DKP}, where they propose five approximation algorithms, the best performing one having approximation ratio $7 + \epsilon$.

\subsubsection{Exact Algorithms}
Exact algorithms offer guaranteed optimality; the computed solution will be an optimal one. These are generally slower than heuristic algorithms, yet in instances with small input it is sometimes more beneficial, as their solution is guaranteed to be optimal. \textit{Dynamic Programming}, mentioned previously, belongs in this category, along with \textit{Branch-and-Bound}.

\textit{Branch-and-bound} is a widely used algorithmic approach used to obtain optimal solutions to combinatorial optimisation problems. A solution tree is explored by the algorithm, and branches of the tree are discarded if, when the branch is checked against an estimated lower and upper bound on the optimal solution, it cannot produce a better solution than the current best. For the mathematical programming methods \textit{Constraint Programming} (CP), \textit{Integer Linear Programming} (ILP) and \textit{Mixed-Integer Linear Programming} (MILP) models\footnote{Ways of representing an optimisation problem, see \cite{IPLectureNotesBerkeley}\cite{wolsey1999integer}\cite{schrijver1998theory}\cite{MIPLecture} for more information on IP, ILP, MIP and MILPs}, solvers usually use the branch-and-bound approach when computing the optimal solution to an input instance. CP is a declarative and logical style of problem formulation; an optimisation problem is defined in terms of logical constraints. Hence CP is sometimes referred to as \textit{Constraint Logic Programming} (CLP) \cite{leung2004handbook}. Integer in ILP refers to all variables being restricted to integers, whilst linear in ILP refers to all constraints and the objective function being of linear form. It is similar for MIP, however the word mixed refers to variables being a mix of integer and continuous types. The generic terms \textit{Integer Programming} (IP) and \textit{Mixed-Integer Programming} (MIP) are often used in literature to refer to either the linear or non-linear variations. IP and MIP are both $\mathbb{NP}$-hard \cite{schrijver1998theory}\cite{IPLectureNotesBerkeley}. An example of a MIP model for the 3D-KP is proposed in Silva et al. \cite{silva2019}, along with a comparison of other author's exact method approaches to the 3D-KP and related variants. This inspired the work of this paper, and influenced the choice of approach when developing the new methods proposed in the subsequent chapter.

\subsection{Motivation} \label{motivation}

KPs are of key interest in both the theoretical and practical applications of optimisation problems \cite{martello1990knapsack}. The theoreticals are drawn to KPs because of their simple structure, allowing new combinatorial methods and techniques to be practiced with relative ease before adapting for harder problems. It is viewed as the simplest ILP problem \cite{martello1990knapsack}. KPs are also often used when solving more complex optimisation problems by abstracting them into knapsack-type sub-problems \cite{martello1990knapsack}\cite{kellerer2004knapsack}, and solving these sub-problems iteratively. In a practical sense, KPs are widely studied in Operations Research, as they can be used to model a large number of problems which arise in industry. Examples in industry include mining \cite{miningKnapsackPaper}, scheduling patients into operating rooms \cite{surgeryKnapsackPaper}, resource allocation for Software as a Service (SaaS) systems \cite{saasKnapsackPaper} and many more. It is not usually the basic model of the KP that is studied in Operations Research and other industry literature; adaptations or variations of the KP are most often used.

Consider now the geometric and higher dimensional variants of the KP; the 2D-KP and 3D-KP (labelled as such by Cacchiani et al. \cite{cacchiani2022knapsackpart2}). Whilst the 2D-KP is a widely studied and famous multidimensional geometric KP (with recent surveys for Exact methods \cite{iori20212DBPP} and Approximation methods \cite{christensen20172DBPP}), it was stated previously in Section \ref{backgroundResearch} that there is limited research on the 3D-KP when compared to the 2D-KP. Recent 3D-KP papers which have been published are typically focused on the 3D-KP or 3D-BPP variants in a real world context, with real world constraints. An example of this can be seen in the very recent article in Computers \& Industrial Engineering by Erbayrak et al. \cite{erbayrak20213DBPP}, which documents a 3D-BPP variant with orientation, item stability and product/item family unity - along with a MIP model.

At the time research for this paper was conducted, almost no publicly or academically available implementations which solve the 3D-KP could be found. Of the implementations investigated, there are only several, such as the GitHub project by Skjølberg \cite{github3DContainerPackingProject}, which are based upon published papers. However these published papers are not by recognisable authors in the field of multidimensional KPs; as an example, the implementation by Skjølberg is based upon the \textit{Largest Area First-Fit} (LAFF) algorithm proposed by Gürbüz et al. \cite{gurbuz20093DBPP}. This appears to be the most publicly accessible and used 3D-KP/3D-BPP implementation, hence it is reasonable to conclude that there is a lack of accessible implementation for the 3D-KP/3D-BPP. It was also evident that no easily accessible and extensible 3D visualisation application was available. Most 3D-KP/3D-BPP visualisers are either commercial software \cite{commercialContainerLoading} or software developed by individual persons for specific personal use cases. The only paper sourced which details a visualisation application is a 2006 article by Dube et al. \cite{dube20063DBPP}; however this visualisation is not only inaccessible for public or academic use, it does not allow loading of solutions from a file, and it is not from a well-known academic source. 

For these reasons, this paper proposes the following:
\begin{itemize}
	\setlength\itemsep{0em}
	\item Two new exact method models; a CP model and a new ILP model. This includes an implementation and computation comparison, along with model definitions. Exact methods were chosen over heuristical approaches because of the scarcity of literature on the 3D-KP (without rotations), regarding implementations of heuristic methods.
	\item A comparison to an implementation of the MIP model proposed by well-known researchers in the 3D-KP/3D-BPP area; the model by Silva et al. in 2019 \cite{silva2019}. This model is adapted slightly to match the same problem as the newly proposed models, ensuring a fair comparison.
	\item A visualisation application which can display outputs from the model implementations, granting the ability to discern feasible solutions and debugging of each model implementation visually.
\end{itemize}