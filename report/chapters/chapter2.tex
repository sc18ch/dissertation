\chapter{Methods}
\label{chapter2}

\section{Solving the 3D-KP} \label{solving3DKP} %TODO Reference constraint programming
The solving approach used in this project uses exact methods: one represented as a CP model, another as a newly proposed ILP model, and an adaptation of a published MIP model to an ILP model. A mathematical representation is provided, along with an implementation of each model in MiniZinc.

\subsubsection{MiniZinc}
\textit{MiniZinc} \cite{minizinc2} is an open source modelling language for constraint programming \cite{minizinc1}, popularly used when solving optimisation or constraint satisfaction problems. The high-level structure of MiniZinc allows abstraction from building complex inequalities with many decision variables, as it's compiler can understand logical implications, conditionals and more. MiniZinc itself does not provide a solver in which to run a completed model with and produce an output; its modelling language hosts interfaces with many popular solvers, one of these being Gurobi \cite{gurobi}, a powerful commercial solver which has support for solving MiniZinc ILP models. Gurobi was selected not only since it's supported by MiniZinc, but also because it claims to be faster than competing solvers, and it provides academic licences.

\subsection{Defining a feasible solution}
If we consider what constitutes as a feasible solution to an instance of a three dimensional orthogonal knapsack problem (no rotations), there are two constraints that must be satisfied:

\begin{enumerate}
	\setlength\itemsep{0em}
	\item No placed box can occupy space outside of the bin
	\item No placed box can occupy the same space as another placed box
\end{enumerate}

By supplying a Constraint Programming or Linear Programming solver with these constraints, along with the objective function of maximising the volume of placed boxes inside the bin, the solver will converge on a valid and optimal solution.

\subsection{Constraint Programming model} \label{CPModelDefinition}

\input{./model_definition/knapsack/knapsack_CP_model_definition}

\subsubsection{MiniZinc implementation}
Modelling in MiniZinc uses the language defined in the MiniZinc Documentation \cite{minizincHandbook}. First, instances can be defined in a \textit{.dzn} MiniZinc file, which can then be used as input for the model. An example of this is seen in Figure \ref{minizincInstanceExample}.

\begin{figure}[!h]
	\begin{minted}[xleftmargin=2em,linenos,frame=lines,autogobble]{c}
n = 2;
itemSizes = [| 75, 75, 75,| 75, 75, 75|];
bin = [100, 100, 100];
	\end{minted}
	\caption{An example \textit{.dzn} file of Instance 1 from Table \ref{table:instances}}
	\label{minizincInstanceExample}
\end{figure}

The model implementation is shown in full at Appendix \ref{CPmodelAppendix}.

\pagebreak
\subsection{New Integer Linear Programming model} \label{ILPModelDefinition}

\input{./model_definition/knapsack/knapsack_ILP_model_definition}

\subsubsection{MiniZinc implementation}
The MiniZinc implementation for the new ILP model can be viewed at Appendix \ref{ILPmodelAppendix}. Note that the same instance input \textit{.dzn} files used for the CP model are also used as input for the new ILP model.
\pagebreak

\subsection{Silva et al. model} \label{SmodelDefinition}
\input{./model_definition/knapsack/silva_model.tex}

\subsubsection{MiniZinc implementation}
The MiniZinc implementation for the S model can be viewed at Appendix \ref{SilvamodelAppendix}. Note that the same instance input \textit{.dzn} files used for the CP model and new ILP model are also used as input for the S model.

\subsection{Output of the models}
Specifying the {\tt --output-mode json} flag when running a MiniZinc model on an instance will save the output solution's decision variable values to a JSON file. The output file extension can be of any format, therefore \textit{.log} was selected for use of solver output files in this project. Since each MiniZinc implementation uses the same decision variable names for item coordinates and whether an item is placed, the visualisation application, discussed in Section \ref{sec:visualisationApp}, needs only one interface to read in these outputs. JSON is a versatile and widely adopted data format; it is therefore relatively trivial to read from these outputs. An example is shown in Figure \ref{minizincOutputExample}.

\begin{figure}[!h]
	\begin{minted}[xleftmargin=2em,linenos,frame=lines,autogobble]{json}
{
  "placed" : [1, 1, 1, 1],
  "itemCoordinates" : [[0, 0, 50], [50, 0, 50], [50, 0, 0], [0, 0, 0]],
  "_objective" : 500000
}
	\end{minted}
	\caption{An extract of an example \textit{.log} file from the CP model running on Instance 3 from Table \ref{table:instances}. {\tt \_objective} is the value of this solution's objective function. With the ILP models, there are more JSON key outputs (e.g. $\alpha_{ij}$ from the new ILP model) due to more decision variables. However only {\tt placed} and {\tt itemCoordinates} are needed for visualisation}
	\label{minizincOutputExample}
\end{figure}

\section{Solution visualisation} \label{sec:visualisationApp}
The solution visualisation, pictured in Figure \ref{visualisationApp}, uses the Java language and the JavaFX \cite{javafxDocs} library to allow generation of instances and loading of solutions produced by the MiniZinc models in Sections \ref{CPModelDefinition} and \ref{ILPModelDefinition}. JavaFX was selected since it offers an extensible platform for visual applications, along with support for three dimensional scenes. Whilst this meant most of the user interface code was developed rapidly and with ease, there are still some elements to development which required significant effort. These include learning and implementing JavaFX, parsing and saving files (I/O), drawing scenes in three dimensional space and various other challenges involved when building a JavaFX application. Other libraries/materials used for this visualisation are specified in Appendix \ref{AppendixExternalMaterial}.

\begin{center}
	\begin{figure}[!h]
		\centering
		\includegraphics[scale=0.3]{visualisation_app}
		\caption{Image of visualisation application, with an Instance 7 (see Table \ref{table:instances}) solution being displayed}
		\label{visualisationApp}
	\end{figure}
\end{center}

\subsection{Uses and features}
The visualisation has the options shown in Table \ref{table:visualisationFeatures}. It also displays efficiency information in the bottom right corner of the application, seen in Figure \ref{visualisationApp}. This includes the percentage of the bin filled, the number of boxes placed, and the percentage of boxes placed.

\vspace{2em}

\begin{table}[!h]
	\renewcommand{\arraystretch}{1.4}
	\centering
	\begin{tabular}{p{0.25\linewidth}p{0.7\linewidth}}
		\toprule
		\textbf{Option} & \textbf{Description} \\
		\midrule
		Save to Data File & Allows the user to save the current loaded or generated instance to a \textit{.dzn} file, which is the file format used to input instances into MiniZinc models \\
		Open Output Log File & Enables the user to load a \textit{.dzn} problem instance, along with a corresponding \textit{.log} file. These \textit{.log} files contain solution outputs from running solvers on the MiniZinc models. Thus, this operation loads a problem solution \\
		Generate New Instance & Produces a series of input screens for instance generation parameters. The generated instance can then be saved to a \textit{.dzn} file using the \textit{Save to Data File} option \\
		Toggle Bin Visibility & Toggles the bin being displayed in the visualisation \\
		Toggle Axis Visibility & Toggles a scene axis indicator being displayed in the visualisation \\
		Box Visibility Toggles & Buttons on the right sidebar allow each individual placed box in the scene to have its visibility toggled. This allows boxes in the centre of the bin to be inspected clearly \\
		\bottomrule
	\end{tabular}
	\caption{Options available in the visualisation application}
	\label{table:visualisationFeatures}
\end{table}

\vspace{1em}

\subsection{Code implementation}
When developing the visualisation application, care was taken to follow established development practices by ensuring code was consistent and well documented. In the Java project directory, there are folders {\tt assets}, {\tt bin}, {\tt lib} and {\tt src}. These folders contain assets for the application, build files, external libraries and source code, respectively. The code is split among packages and classes in {\tt src}, where its code structure is shown in Figure \ref{VisualisationAppDirectoryTree}. Splitting the code into separate packages is beneficial since it improves the reusability of code among the project or future projects. The current structure provides abstraction from the implementation of individual classes, and therefore promotes good software engineering practices.

\begin{figure}[!h]
	\dirtree{%
	.1 src.
	.2 components.
	.3 BoxItem\DTcomment{class for logical representation of a 3D Box}.
	.3 BoxItemList\DTcomment{class to represent a list of {\tt BoxItem}}.
	.3 Container\DTcomment{class for logical representation of a 3D Bin/Container}.
	.3 PackingInstance\DTcomment{class for a packing instance; a {\tt BoxItemList} and a {\tt Container}}.
	.2 gui.
	.3 ApplicationGUI\DTcomment{class to handle the application window}.
	.3 OptionsBarGUI\DTcomment{class for options sidebar on the right hand side of the application}.
	.2 io.
	.3 DataFile\DTcomment{static class to load and save a {\tt PackingInstance} from and to a \textit{.dzn} file}.
	.3 LPOutputFile\DTcomment{class to load MiniZinc solver output files \textit{.log} into the 3D scene}.
	.2 main.
	.3 Main\DTcomment{class used to launch the application}.
	.2 maths.
	.3 Coordinate\DTcomment{class used to represent a coordinate in 3D space}.
	.2 scene.
	.3 Axis\DTcomment{class to represent the visual axis in the 3D scene}.
	.3 BinVisual\DTcomment{class to represent the visual of the Bin/Container in the scene}.
	.3 MainScene\DTcomment{class to represent and handle the 3D scene}.
	}
	\caption{Directory tree of {\tt src}, defining package and class structure of visualisation application}
	\label{VisualisationAppDirectoryTree}
\end{figure}

\vspace{2em}

%E.g. testing, version control, agile methodology etc.
\section{Project management and methodologies} \label{projectMethodologies}
Version control was used heavily in this project due to a number of factors. It provided a method of working from multiple machines without the need to transfer a physical copy of the project or a digital one via email, drive storage etc. It also meant a backup of past versions of the project in the event of data loss or corruption on a local machine. Finally, workflow was increased by enabling comparison of previous project versions to more recent versions. The GitLab repository for this project can be found at Appendix \ref{versionControlAppendix}.

Bash scripts were created to setup, build and run the visualisation application, as well as run scripts for the MiniZinc implementations. These proved invaluable for the workflow when working on this project, as they provide single commands to launch, yet take care of the setup/build/run details needed for compiling and running the application and models. These scripts can be viewed at Appendix \ref{AppendixBashScripts}.

To perform tests on the MiniZinc implementations, a custom Python script was developed to make creating test instances effortless. Creating some of the \textit{.dzn} test instances, for example instances with over 100 boxes, would be a labouring and menial task. The Python script which remedies this can be seen in full at Appendix \ref{AppendixPythonScripts}.
