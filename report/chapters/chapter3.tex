\chapter{Results}
\label{chapter3}

\section{Implemented models}

\subsection{Hypotheses} \label{ssec:hypothesis}

\begin{center}	
	\begin{tabular}{p{0.2\linewidth}p{0.6\linewidth}}
		\textbf{Hypothesis 1} & As the number of boxes increases, the average computation time to find an optimal solution should rise exponentially for all models \\
		\\
		\textbf{Hypothesis 2} & The CP model will perform better with computation time on average than the ILP models when finding an optimal solution, using the test instances \\
		\\
		\textbf{Hypothesis 3} & The model with the least constraints and decision variables will perform the best in computation time on average when determining an optimal solution, using the test instances \\
	\end{tabular}
\end{center}

\textbf{Hypothesis 1} is proposed since, as the number of boxes increases, so will the amount of permutations of selected boxes and their positions in the container. This subsequently produces a solution tree which grows exponentially as the number of boxes increases; therefore the computation time should rise exponentially. This should also be reflected by the verity that 3D-KP is $\mathbb{NP}$-hard.

\textbf{Hypothesis 2} is expected since MiniZinc and Gurobi support logical implications as constraints instead of linear programming constraints. Thus, it is assumed optimisations are in place by MiniZinc and Gurobi when using these features, which may result in a smaller solution tree.

\textbf{Hypothesis 3} is made with similar presumptions as hypothesis 1 in that less variables results in fewer possible permutations, thus a smaller solution tree. Less constraints also means less checks when the model determines if a proposed solution is feasible. The CP model contains $4n$ variables and 6 constraints, the new ILP model contains $21n^2 + 4n$ variables and 29 constraints, and the S model contains $6n^2 + 4n$ variables and 13 constraints\footnote{Variable counts calculated from previously defined model definitions, where $n$ is the number of items}. Therefore, based on this and on hypothesis 2, it is expected that the CP model is best performing, and the new ILP model is worst performing.

\subsection{Running Experiments} \label{runtimeExperiments}
The solver used when performing these experiments was Gurobi, mentioned previously in Section \ref{solving3DKP}. When testing the MiniZinc models to ensure that the output was a feasible solution, the instances in Table \ref{table:instances} were created and used. Since the optimal solution can be derived logically for most of these instances, the output of the models can be compared to the expected optimal solutions. Tables \ref{table:LIModelTests}, \ref{table:ILPModelTests} and \ref{table:SModelTests} show for each model whether it produces a feasible solution and whether that solution is optimal for a particular instance. Note that these tests were performed on a Desktop computer, with the specifications defined in Appendix \ref{computerSpecs}. Each test was granted a maximum runtime of 3600 seconds before declaring the test as 'Did not finish'.

\begin{table}[h]
	\renewcommand{\arraystretch}{1.2}
	\centering
	\begin{tabular}{|P{0.1\linewidth}|P{0.2\linewidth}|P{0.45\linewidth}|P{0.15\linewidth}|}
		\hline
		\textbf{Number} & \textbf{Name} & \textbf{Description} & \textbf{Bin Size} [$x,y,z$] \\ \hline
		\textbf{1} & Two Overlapping Boxes & 2 identical boxes which have sizes larger than half size of the bin. This instance checks that a model does not produce an infeasible solution where the two boxes overlap & [$100,100,100$] \\ \hline
		\textbf{2} & Over Half Bin Size Boxes & 7 different sized boxes which all have sizes larger than half the size of the bin. An optimal solution placed the largest box as the one placed box in the bin & [$700,500,600$] \\ \hline
		\textbf{3} & Full Fit 4 Boxes & 4 identical boxes with [$x,y,z$] sizes of [$50,50,50$] which can all perfectly fit inside the bin & [$100,50,100$] \\ \hline
		\textbf{4} & Full Fit 8 Boxes & As above but with 8 boxes & [$100,100,100$] \\ \hline
		\textbf{5} & Full Fit 12 Boxes & As above but with 12 boxes & [$100,150,100$] \\ \hline
		\textbf{6} & Full Fit 16 Boxes & As above but with 16 boxes & [$100,200,100$] \\ \hline
		\textbf{7} & Full Fit 20 Boxes & As above but with 20 boxes & [$100,250,100$] \\ \hline
		\textbf{8} & Full Fit 24 Boxes & As above but with 24 boxes & [$100,300,100$] \\ \hline
		\textbf{9} & Full Fit 28 Boxes & As above but with 28 boxes & [$100,350,100$] \\ \hline
		\textbf{10} & Full Fit 32 Boxes & As above but with 32 boxes & [$200,100,200$] \\ \hline
		\textbf{11} & Full Fit 48 Boxes & As above but with 48 boxes & [$200,300,100$] \\ \hline
		\textbf{12} & Full Fit 64 Boxes & As above but with 64 boxes & [$200,200,200$] \\ \hline
		\textbf{13} & Full Fit 100 Boxes & As above but with 100 boxes & [$250,200,250$] \\ \hline
		\textbf{14} & Full Fit 125 Boxes & As above but with 125 boxes & [$250,250,250$] \\ \hline
		\textbf{15} & Full Fit 175 Boxes & As above but with 175 boxes & [$250,250,350$] \\ \hline
		\textbf{16} & Full Fit 180 Boxes & As above but with 180 boxes & [$300,250,300$] \\ \hline
		\textbf{17} & Random 15 Boxes & 15 random sized boxes where the total volume of boxes is near to the volume of the bin. The optimal solution to this instance is not known & [$500,500,500$] \\ \hline
		\textbf{18} & Random 20 Boxes & 20 random sized boxes where the total volume of boxes is near to the volume of the bin. The optimal solution to this instance is not known & [$800,400,300$] \\ \hline
	\end{tabular}
	\caption[footnote]{List of instances used to test model validity\footnotemark}
	\label{table:instances}
\end{table}
\footnotetext{These test instances can be viewed in full in directory {\tt solver/instances/knapsack} from the repository in Appendix \ref{versionControlAppendix}}

\begin{table}[h]
	\centering
	\begin{tabular}{|c|cc|cc|c|}
		\hline
		\multirow{2}{*}{\textbf{Instance}} & \multicolumn{2}{c|}{\textbf{Solution Output}} & \multicolumn{2}{c|}{\textbf{Bin capacity filled} (Efficiency)} & \multirow{2}{*}{\textbf{Runtime} $t$ (Avg. of 3)} \\ \cline{2-5} 
		& \multicolumn{1}{c|}{Feasible} & Optimal & \multicolumn{1}{c|}{Model Output} & Optimal & \\ \hline
		\textbf{1} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$42.19\%$} & $42.19\%$ &  136ms\\ \hline
		\textbf{2} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$15.19\%$} & $15.19\%$ & 8m 22s\\ \hline
		\textbf{3} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 148ms\\ \hline
		\textbf{4} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 327ms\\ \hline
		\textbf{5} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 2s 720ms\\ \hline
		\textbf{6} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 4s 819ms\\ \hline
		\textbf{7} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 17s 361ms\\ \hline
		\textbf{8} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 1m 17s\\ \hline
		\textbf{9} & \multicolumn{1}{c|}{N/A} & N/A & \multicolumn{1}{c|}{N/A} & $100\%$ & Did not finish\\ \hline
		\textbf{10} & \multicolumn{1}{c|}{N/A} & N/A & \multicolumn{1}{c|}{N/A} & $100\%$ & Did not finish\\ \hline
		\textbf{11} & \multicolumn{1}{c|}{N/A} & N/A & \multicolumn{1}{c|}{N/A} & $100\%$ & Did not finish\\ \hline
		\textbf{12} & \multicolumn{1}{c|}{N/A} & N/A & \multicolumn{1}{c|}{N/A} & $100\%$ & Did not finish\\ \hline
		\textbf{13} & \multicolumn{1}{c|}{N/A} & N/A & \multicolumn{1}{c|}{N/A} & $100\%$ & Did not finish\\ \hline
		\textbf{14} & \multicolumn{1}{c|}{N/A} & N/A & \multicolumn{1}{c|}{N/A} & $100\%$ & Did not finish\\ \hline
		\textbf{15} & \multicolumn{1}{c|}{N/A} & N/A & \multicolumn{1}{c|}{N/A} & $100\%$ & Did not finish\\ \hline
		\textbf{16} & \multicolumn{1}{c|}{N/A} & N/A & \multicolumn{1}{c|}{N/A} & $100\%$ & Did not finish\\ \hline
		\textbf{17} & \multicolumn{1}{c|}{Y} & Unknown & \multicolumn{1}{c|}{$72.80\%$} & Unknown & 3m 40s\\ \hline
		\textbf{18} & \multicolumn{1}{c|}{N/A} & Unknown & \multicolumn{1}{c|}{N/A} & Unknown & Did not finish\\ \hline
	\end{tabular}
	\caption{Test results for the CP model from \ref{CPModelDefinition}, where Y indicates 'Yes' and N indicates 'No'}
	\label{table:LIModelTests}
\end{table}

\begin{table}[h]
	\centering
	\begin{tabular}{|c|cc|cc|c|}
		\hline
		\multirow{2}{*}{\textbf{Instance}} & \multicolumn{2}{c|}{\textbf{Solution Output}} & \multicolumn{2}{c|}{\textbf{Bin capacity filled} (Efficiency)} & \multirow{2}{*}{\textbf{Runtime} $t$ (Avg. of 3)} \\ \cline{2-5} 
		& \multicolumn{1}{c|}{Feasible} & Optimal & \multicolumn{1}{c|}{Model Output} & Optimal & \\ \hline
		\textbf{1} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$42.19\%$} & $42.19\%$ & 106ms\\ \hline
		\textbf{2} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$15.19\%$} & $15.19\%$ & 542ms\\ \hline
		\textbf{3} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 118ms\\ \hline
		\textbf{4} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 200ms\\ \hline
		\textbf{5} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 452ms\\ \hline
		\textbf{6} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 786ms\\ \hline
		\textbf{7} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 1s 232ms\\ \hline
		\textbf{8} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 1s 752ms\\ \hline
		\textbf{9} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 5s 256ms\\ \hline
		\textbf{10} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 6s 142ms\\ \hline
		\textbf{11} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 1m 54s\\ \hline
		\textbf{12} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 17m 56s\\ \hline
		\textbf{13} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 40m 37s\\ \hline
		\textbf{14} & \multicolumn{1}{c|}{N/A} & N/A & \multicolumn{1}{c|}{N/A} & $100\%$ & Did not finish\\ \hline
		\textbf{15} & \multicolumn{1}{c|}{N/A} & N/A & \multicolumn{1}{c|}{N/A} & $100\%$ & Did not finish\\ \hline
		\textbf{16} & \multicolumn{1}{c|}{N/A} & N/A & \multicolumn{1}{c|}{N/A} & $100\%$ & Did not finish\\ \hline
		\textbf{17} & \multicolumn{1}{c|}{Y} & Unknown & \multicolumn{1}{c|}{$72.80\%$} & Unknown & 2s 382ms\\ \hline
		\textbf{18} & \multicolumn{1}{c|}{Y} & Unknown & \multicolumn{1}{c|}{$70.78\%$} & Unknown & 1m 45s\\ \hline
	\end{tabular}
	\caption{Test results for the new ILP model from \ref{ILPModelDefinition}, where Y indicates 'Yes' and N indicates 'No'}
	\label{table:ILPModelTests}
\end{table}

\begin{table}[h]
	\centering
	\begin{tabular}{|c|cc|cc|c|}
		\hline
		\multirow{2}{*}{\textbf{Instance}} & \multicolumn{2}{c|}{\textbf{Solution Output}} & \multicolumn{2}{c|}{\textbf{Bin capacity filled} (Efficiency)} & \multirow{2}{*}{\textbf{Runtime} $t$ (Avg. of 3)} \\ \cline{2-5} 
		& \multicolumn{1}{c|}{Feasible} & Optimal & \multicolumn{1}{c|}{Model Output} & Optimal & \\ \hline
		\textbf{1} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$42.19\%$} & $42.19\%$ &  104ms\\ \hline
		\textbf{2} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$15.19\%$} & $15.19\%$ & 142ms\\ \hline
		\textbf{3} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 102ms\\ \hline
		\textbf{4} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 131ms\\ \hline
		\textbf{5} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 148ms\\ \hline
		\textbf{6} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 179ms\\ \hline
		\textbf{7} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 208ms\\ \hline
		\textbf{8} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 270ms\\ \hline
		\textbf{9} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 323ms\\ \hline
		\textbf{10} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 1s 538ms\\ \hline
		\textbf{11} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 6s 464ms\\ \hline
		\textbf{12} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 1m 30s\\ \hline
		\textbf{13} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 2m 9s\\ \hline
		\textbf{14} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 17m 36s\\ \hline
		\textbf{15} & \multicolumn{1}{c|}{Y} & Y & \multicolumn{1}{c|}{$100\%$} & $100\%$ & 33m 38s\\ \hline
		\textbf{16} & \multicolumn{1}{c|}{N/A} & N/A & \multicolumn{1}{c|}{N/A} & $100\%$ & Did not finish\\ \hline
		\textbf{17} & \multicolumn{1}{c|}{Y} & Unknown & \multicolumn{1}{c|}{$72.80\%$} & Unknown & 1s 253ms\\ \hline
		\textbf{18} & \multicolumn{1}{c|}{Y} & Unknown & \multicolumn{1}{c|}{$70.78\%$} & Unknown & 7s 802ms\\ \hline
	\end{tabular}
	\caption{Test results for the S model from \ref{SmodelDefinition}, where Y indicates 'Yes' and N indicates 'No'}
	\label{table:SModelTests}
\end{table}
\FloatBarrier

% Comparing runtime against number of items
\centerline{\begin{minipage}[t]{0.5\textwidth}
	\begin{figure}[H]
		\centering
		\begin{tikzpicture}[scale=0.9]
			\begin{axis}[
				xlabel={Number of items $n$},
				ylabel={Avg. Runtime $t$ ($s$)},
				xmin=4, xmax=200,
				ymax=3600,
				xtick={4,8,16,32,64,125,200},
				ytick={0.1,1,60,1000,3600},
				legend pos=north west,
				ymajorgrids=true,
				grid style=dashed,
				xmode=log,
				ymode=log,
				log ticks with fixed point,
				legend style={nodes={scale=0.8, transform shape}}
				]
				
				\addplot[
				color=red,
				mark=*
				]
				coordinates {
					(4,0.148)(8,0.327)(12,2.720)(16,4.819)(20,17.361)(24,77)
				};
				
				\addplot[smooth,
				color=orange,
				dashed,
				thick
				]
				coordinates {
					(4,0.135)(8,0.472)(12,1.646)(16,5.748)(20,20.065)(24,70.047)(28,244.534)(36.605,3600)
				};
				
				\addplot[
				color=blue,
				mark=*
				]
				coordinates {
					(4,0.118)(8,0.200)(12,0.452)(16,0.786)(20,1.232)(24,1.752)(28,5.256)(32,6.142)(48,114)(64,1076)(100,2437)
				};
				
				\addplot[smooth,
				color=cyan,
				dashed,
				thick
				]
				coordinates {
					(4,0.222)(8,0.35)(12,0.552)(16,0.871)(20,1.374)(28,3.416)(32,5.387)(48,33.307)(64,205.945)(80,1273.387)(89.127,3600)
				};
				
				\addplot[
				color=black!35!green,
				mark=*
				]
				coordinates {
					(4,0.102)(8,0.131)(12,0.148)(16,0.179)(20,0.208)(24,0.27)(28,0.323)(32,1.538)(48,6.464)(64,90)(100,129)(125,1056)(175,2018)
				};
				
				\addplot[smooth,
				color=black!15!green,
				dashed,
				thick
				]
				coordinates {
					(4,0.135)(8,0.177)(12,0.231)(16,0.301)(20,0.393)(28,0.67)(32,0.875)(48,2.546)(64,7.405)(100,81.792)(125,433.664)(150,2299.302)(156.719,3600)
				};
				\legend{CP model,,New ILP model,,S model,} 
			\end{axis}
		\end{tikzpicture}
		\caption{Computation time $t$ in seconds against number of items $n$ input (logarithmic axis scaling)}
		\label{figure:RuntimeItemsLogarithmic}
	\end{figure}
\end{minipage}\hspace*{0pc}%Increase the space how much you like
\begin{minipage}[t]{0.5\textwidth}
	\begin{figure}[H]
		\begin{tikzpicture}[scale=0.9]
			\begin{axis}[
				xlabel={Number of items $n$},
				ylabel={Avg. Runtime $t$ ($s$)},
				xmin=4, xmax=175,
				ymax=2500,
				xtick={4,16,32,64,125,175},
				ytick={0,1000,2500},
				legend pos=north west,
				ymajorgrids=true,
				grid style=dashed,
				%xmode=log,
				log ticks with fixed point,
				legend style={nodes={scale=0.8, transform shape}}
				]
				
				\addplot[
				color=red,
				mark=*
				]
				coordinates {
					(4,0.148)(8,0.327)(12,2.720)(16,4.819)(20,17.361)(24,77)
				};
				
				\addplot[smooth,
				color=orange,
				dashed,
				thick
				]
				coordinates {
					(4,0.135)(8,0.472)(12,1.646)(16,5.748)(20,20.065)(24,70.047)(28,244.534)(36.605,3600)
				};
				
				\addplot[
				color=blue,
				mark=*
				]
				coordinates {
					(4,0.118)(8,0.200)(12,0.452)(16,0.786)(20,1.232)(24,1.752)(28,5.256)(32,6.142)(48,114)(64,1076)(100,2437)
				};
				
				\addplot[smooth,
				color=cyan,
				dashed,
				thick
				]
				coordinates {
					(4,0.222)(8,0.35)(12,0.552)(16,0.871)(20,1.374)(28,3.416)(32,5.387)(48,33.307)(64,205.945)(80,1273.387)(89.127,3600)
				};
				
				\addplot[
				color=black!35!green,
				mark=*
				]
				coordinates {
					(4,0.102)(8,0.131)(12,0.148)(16,0.179)(20,0.208)(24,0.27)(28,0.323)(32,1.538)(48,6.464)(64,90)(100,129)(125,1056)(175,2018)
				};
				
				\addplot[smooth,
				color=black!15!green,
				dashed,
				thick
				]
				coordinates {
					(4,0.135)(8,0.177)(12,0.231)(16,0.301)(20,0.393)(28,0.67)(32,0.875)(48,2.546)(64,7.405)(100,81.792)(125,433.664)(150,2299.302)(156.719,3600)
				};
				\legend{CP model,,New ILP model,,S model,} 
			\end{axis}
		\end{tikzpicture}
		\caption{Computation time $t$ in seconds against number of items $n$ input (linear axis scaling)}
		\label{figure:RuntimeItemsLinear}
	\end{figure}  
\end{minipage}}

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		
		\begin{axis} [ybar = .05cm,
			bar width = 4pt,
			xlabel={Instance number},
			ylabel={Avg. Runtime $t$ ($s$)},
			xmin = 0.5, xmax = 18.5, 
			ymax=3600,
			xtick={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18},
			ytick={0.1,1,60,1000,3600},		
			ymode=log,
			log ticks with fixed point,
			log origin=infty,
			legend style={at={(0.18,0.8)},anchor=west,nodes={scale=0.8, transform shape}},
			width=\columnwidth,
			height=18em
			]
			
			\addplot[color=red,fill=red] coordinates {
				(1,0.136)(2,502)(3,0.148)(4,0.327)(5,2.720)(6,4.819)(7,17.361)(8,77)(17,220)
			};
			\addlegendentry{CP model}
			
			\addplot[color=blue,fill=blue] coordinates {
				(1,0.106)(2,0.542)(3,0.118)(4,0.200)(5,0.452)(6,0.786)(7,1.232)(8,1.752)(9,5.256)(10,6.142)(11,114)(12,1076)(13,2437)(17,2.382)(18,105)
			};
			\addlegendentry{New ILP model}
			
			\addplot[color=black!35!green,fill=black!35!green] coordinates {
				(1,0.104)(2,0.142)(3,0.102)(4,0.131)(5,0.148)(6,0.179)(7,0.208)(8,0.27)(9,0.323)(10,1.538)(11,6.464)(12,90)(13,129)(14,1056)(15,2018)(17,1.253)(18,7.802)
			};
			\addlegendentry{S model}
			
		\end{axis}
	\end{tikzpicture}
	\caption{Computation time $t$ against test instances (logarithmic axis scaling)}
	\label{figure:RuntimeInstancesLogarithmic}
\end{figure}

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		
		\begin{axis} [ybar = .05cm,
			bar width = 4pt,
			xlabel={Instance number},
			ylabel={Avg. Runtime $t$ ($s$)},
			xmin = 0.5, xmax = 18.5, 
		    ymax=2500,
			xtick={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18},
			ytick={0,1000,2500},		
			%ymode=log,
			log ticks with fixed point,
			log origin=infty,
			legend style={at={(0.18,0.8)},anchor=west,nodes={scale=0.8, transform shape}},
			width=\columnwidth,
			height=17em
			]
			
			\addplot[color=red,fill=red] coordinates {
				(1,0.136)(2,502)(3,0.148)(4,0.327)(5,2.720)(6,4.819)(7,17.361)(8,77)(17,220)
			};
			\addlegendentry{CP model}
			
			\addplot[color=blue,fill=blue] coordinates {
				(1,0.106)(2,0.542)(3,0.118)(4,0.200)(5,0.452)(6,0.786)(7,1.232)(8,1.752)(9,5.256)(10,6.142)(11,114)(12,1076)(13,2437)(17,2.382)(18,105)
			};
			\addlegendentry{New ILP model}
			
			\addplot[color=black!35!green,fill=black!35!green] coordinates {
				(1,0.104)(2,0.142)(3,0.102)(4,0.131)(5,0.148)(6,0.179)(7,0.208)(8,0.27)(9,0.323)(10,1.538)(11,6.464)(12,90)(13,129)(14,1056)(15,2018)(17,1.253)(18,7.802)
			};
			\addlegendentry{S model}
		\end{axis}
	\end{tikzpicture}
	\caption{Computation time $t$ against test instances (linear axis scaling)}
	\label{figure:RuntimeInstancesLinear}
\end{figure}
\FloatBarrier

\begin{table}[h]
	\renewcommand{\arraystretch}{1.2}
	\centering
	\begin{tabular}{P{0.18\linewidth}P{0.40\linewidth}P{0.38\linewidth}}
		\toprule
		\textbf{Model} & \textbf{Exponential Regression Function} & \textbf{Product Moment Correlation Coefficient} \\
		\midrule
		CP model & $t = 0.0387 \cdot 1.3669^n$ & $r = 0.9918$ \\
		New ILP model & $t = 0.1409 \cdot 1.1206^n$ & $r = 0.9645$ \\
		S model & $t = 0.1035 \cdot 1.069^n$ & $r = 0.9563$ \\
		\bottomrule
	\end{tabular}
	\caption[footnote]{Exponential regression functions for each model, shown in Figure \ref{figure:RuntimeItemsLogarithmic} and \ref{figure:RuntimeItemsLinear} as dashed lines. Here $r$ represents regression accuracy\footnotemark}
	\label{table:ExponentialRegressions}
\end{table}
\footnotetext{Where $r=1$ indicates a regression line which matches all the data points and $r=0$ indicates a regression line with no correlation to the data points. These calculations were performed using a Casio™ fx-991EX Classwiz}

\vspace{1em}

The results from Figure \ref{figure:RuntimeItemsLogarithmic}, \ref{figure:RuntimeItemsLinear}, \ref{figure:RuntimeInstancesLogarithmic} and \ref{figure:RuntimeInstancesLinear}, along with the regression lines calculated in Table \ref{table:ExponentialRegressions}, indicate that the computation time for all three models increases exponentially as the number of inputs (boxes) is increased. Since the exponential regression functions defined in Table \ref{table:ExponentialRegressions} have $r$ correlation coefficients close to $1$, it can be stated that the exponential functions accurately represent the trend of data for each model; hence this confirms the statement made in \textbf{Hypothesis 1} previously.

\vspace{1em}

\subsection{Model comparisons} \label{modelComparisons}
Surprisingly, from the results, \textbf{Hypothesis 2} was not confirmed. One possible cause of this is the use of Big $M$ in the ILP models, and lack thereof in the CP model. When a MiniZinc model is compiled into one understood by the solver Gurobi, the constraints formulated as logical implications in the CP model are converted to Gurobi's "General Constraints" \cite{gurobiDocsConstraints}. From the Gurobi documentation, it is stated that using the "General Constraint" feature for indicator constraints can come at the expense of additional processing time \cite{gurobiDocsBigM} \cite{gurobiNumericalWebinar}. The exact cause of this could not be found, it is likely a question specific to the implementation of Gurobi itself\footnote{\label{gurobiFootnote}Since Gurobi is a commercial product, specific implementation details about the solver are unavailable}. However it is highly likely that the these "General Constraints" are using automatically generated decision variables and/or constraints, as well as a Big $M$-esque bound when solving. It is probable that these are based from a model's constraints and/or inputs. This could result in an $M$ value greater than the ones defined previously in the ILP models, hence causing a looser bound which can lead to poor pruning of solution trees when the solver is performing Branch-and-Bound \cite{bigMPerils}.

Regarding \textbf{Hypothesis 3}, it could be argued that this was confirmed. The exact number of automatically generated decision variables and constraints when using Gurobi's "General Constraints" cannot be known. Thus, it is entirely possible that the CP model does indeed have more decision variables and/or constraints than the ILP models, and is therefore the worst performing. To further test this hypothesis, consider the new ILP model versus the S model. As noted in Section \ref{ssec:hypothesis}, the S model has less decision variables and constraints than the new ILP model. The average computation time of the S model outperforms that of the new ILP model, hence, given the uncertainty of the amount of decision variables and constraints in the CP model, this hypothesis is confirmed. 