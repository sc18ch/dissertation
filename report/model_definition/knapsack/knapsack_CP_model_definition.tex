The \textit{Constraint Programming} (CP) model, implemented in MiniZinc, is subsequently defined using logical implications to represent the implementation in MiniZinc. This formulation represents a three-dimensional orthogonal geometric knapsack problem in which items are placed inside a single bin of fixed size, such that the occupied space inside the bin is maximised. The inputs in Table \ref{table:CPInputs} and variables in Table \ref{table:CPVariables} associated with the bin and items form a cuboid or 'box' in three dimensional space. The problem formulation is defined by the equations in Figure \ref{figure:CPEquations}.

\begin{table}[h]
	\renewcommand{\arraystretch}{1.2}
	\centering
	\begin{tabular}{p{0.13\linewidth}p{0.50\linewidth}p{0.24\linewidth}}
		\toprule
		\textbf{Input} && \textbf{Domain} \\
		\midrule
		$n$ & the number of items & $n \in \mathbb{N}, n > 1$ \\
		$I$ & the set of items to be packed in the bin & $I = \{1, ..., n\}$ \\
		$p_i$, $q_i$, $r_i$ & $x$, $y$ and $z$ size of item $i$, respectively & $p_i, q_i, r_i \in \mathbb{Z}^{+}$, $\forall i \in I$ \\
		$P, Q, R$ & $x$, $y$ and $z$ size of the bin, respectively & $P, Q, R \in \mathbb{Z}^{+}$ \\
		\bottomrule
	\end{tabular}
	\caption{Inputs for the CP model}
	\label{table:CPInputs}
\end{table}

\begin{table}[h]
	\renewcommand{\arraystretch}{1.2}
	\centering
	\begin{tabular}{p{0.13\linewidth}p{0.77\linewidth}}
		\toprule
		\textbf{Variable} & \\
		\midrule
		$s_i$ & binary variable which equals 1 if item $i$ is placed in the bin and 0 otherwise \\
		$x_i, y_i, z_i$ & integer variables indicating coordinates of left-bottom-front corner of item $i$, given in respect to $x$, $y$ and $z$ \\
		\bottomrule
	\end{tabular}
	\caption{Variables for the CP model}
	\label{table:CPVariables}
\end{table}

\begin{figure}[h]
	\renewcommand{\arraystretch}{1.2}
	\centering
	\begin{mdframed}
		\begin{tabular}{p{0.11\linewidth}p{0.6\linewidth}p{0.17\linewidth}p{0.02\linewidth}}
			Maximise & $\mathlarger{\mathlarger{\sum}_{i=1}^{n}}s_i \cdot p_i \cdot q_i \cdot r_i$ & & (1) \\
			Subject to &&& \\
			& $s_i \Rightarrow x_i + p_i \leq P$, & $\forall i \in I$, & (2) \\
			& $s_i \Rightarrow y_i + q_i \leq Q$, & $\forall i \in I$, & (3) \\
			& $s_i \Rightarrow z_i + r_i \leq R$, & $\forall i \in I$, & (4) \\
			&&&\\
			Let $A$ be: & $((x_i \leq x_j) \wedge (x_j < x_i + p_i)) \vee ((x_j \leq x_i) \wedge (x_i < x_j + p_j))$, & & \\
			Let $B$ be: & $((y_i \leq y_j) \wedge (y_j < y_i + q_i)) \vee ((y_j \leq y_i) \wedge (y_i < y_j + q_j))$, & & \\
			Let $C$ be: & $((z_i \leq z_j) \wedge (z_j < z_i + r_i)) \vee ((z_j \leq z_i) \wedge (z_i < z_j + r_j))$, & & \\
			Then & $s_i \wedge s_j \Rightarrow \neg (A \wedge B \wedge C)$, &$\forall i,j \in I$, $i \neq j$, & (5)\\
			\\
			& $s_i \in \{0, 1\}$, & $\forall i \in I$, & (6) \\
			& $x_i$, $y_i$, $z_i \geq 0$, & $\forall i \in I$ & (7) \\
		\end{tabular}
	\end{mdframed}
	\caption{Problem formulation for the CP model}
	\label{figure:CPEquations}
\end{figure}

\begin{table}[h]
	\centering
	\begin{tabular}{p{0.13\linewidth}p{0.77\linewidth}}
		\toprule
		\textbf{Input} & \textbf{Value} \\
		\midrule
		$n$ & $2$ \\
		$I$ & $\{1, 2\}$ \\
		$p_1, q_1, r_1$ & $4, 4, 3$ \\
		$p_2, q_2, r_2$ & $3, 4, 3$ \\
		$P, Q, R$ & $4, 4, 4$ \\
		\bottomrule
	\end{tabular}
	\caption{Example instance, see Appendix \ref{CPmodelAppendix} for a step-by-step walkthrough}
	\label{table:exerciseInstance}
\end{table}

\FloatBarrier
From Figure \ref{figure:CPEquations}, \textbf{Equation 1} defines the Objective Function of the problem; to maximise the occupied space within the bin.

\textbf{Equations 2-4} are bounds for item coordinates, ensuring no placed item can be located outside the boundaries of the bin. For example with Constraint 2, this is ensured in dimension $x$ by checking that the item's coordinate $x_i$ plus it's size $p_i$ is less than or equal to the size of the bin $P$ (and likewise for dimensions $y$ and $z$).

\textbf{Equation 5} enforces that every placed item $i$ cannot overlap in $x$, $y$ and $z$ with another placed item $j$, using the $A$, $B$ and $C$ implications. $A$, $B$ and $C$ indicate true if an overlap is detected in dimensions $x$, $y$ and $z$ respectively. Equation 5 works as displayed in Figure \ref{CPModelConstraint4Figure} for dimension $x$.

\vspace{2em}

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		\usetikzlibrary{arrows.meta}
		
		\draw [{Circle[length=0.5em, width=0.5em]}-{Circle[length=0.5em, width=0.5em]}] node[left, anchor=north] {$x_i$} (0,0) -- (2.5,0) node[right, anchor=north] {$x_i+p_i$};
		\draw [|-|, dashed] (0.05,0.5) -- (2.45,0.5) node[midway, anchor=south] {$x_j$};
		\draw (1.4,-1.7) node[align=center, text width=14em] {Range of $x_j$ values where: $x_i \leq x_j \wedge x_j < x_i + p_i \Rightarrow A=1$};
		
		\draw [{Circle[length=0.5em, width=0.5em]}-{Circle[length=0.5em, width=0.5em]}] (0+7,0) node[left, anchor=north] {$x_j$} -- (2.5+7,0) node[right, anchor=north] {$x_j+p_j$};
		\draw [|-|, dashed] (0.05+7,0.5) -- (2.45+7,0.5) node[midway, anchor=south] {$x_i$};
		\draw (1.4+7,-1.7) node[align=center, text width=14em] {Range of $x_i$ values where: $x_j \leq x_i \wedge x_i < x_j + p_j \Rightarrow A=1$};
		
	\end{tikzpicture}
	\vspace{1em}
	\caption{Constraint 5 in a visual representation, showing that an overlap in the $x$ dimension implies $A=1$}
	\label{CPModelConstraint4Figure}
\end{figure}

\vspace{1em}

Finally, \textbf{Equations 6-7} provide domains for the decision variables.

These constraints use $s_i$ and $s_j$ to ensure that they only apply when an item is deemed as 'placed'. Thus, from this model definition, it is clear to see how a solution which complies with the specified constraints is indeed a feasible solution. To verify whether this model's constraints can produce a feasible solution for the instance in Table \ref{table:exerciseInstance}, see Appendix \ref{CPmodelAppendix} for a walkthrough.

\vspace{3em}