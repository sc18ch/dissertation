The new \textit{Integer Linear Programming} (ILP) model, implemented in MiniZinc, is defined below. This formulation models the exact problem presented previously in Section \ref{CPModelDefinition}, yet it does not use any logical implications like those in the CP model, and is therefore a true ILP model. It's constraints however are equivalent in terms of method to those proposed in Section \ref{CPModelDefinition}.

\vspace{3em}

\begin{table}[!h]
	\renewcommand{\arraystretch}{1.2}
	\centering
	\begin{tabular}{p{0.13\linewidth}p{0.50\linewidth}p{0.24\linewidth}}
		\toprule
		\textbf{Input} && \textbf{Domain} \\
		\midrule
		$n$ & the number of items & $n \in \mathbb{N}, n > 1$ \\
		$I$ & the set of items to be packed in the bin & $I = \{1, ..., n\}$ \\
		$p_i$, $q_i$, $r_i$ & $x$, $y$ and $z$ size of item $i$, respectively & $p_i, q_i, r_i \in \mathbb{Z}^{+}$, $\forall i \in I$ \\
		$P, Q, R$ & $x$, $y$ and $z$ size of the bin, respectively & $P, Q, R \in \mathbb{Z}^{+}$ \\
		$M$ & large integer constant & $M = \max{(P, Q, R)} + 1$ \\
		\bottomrule
	\end{tabular}
	\caption{Inputs for the new ILP model}
	\label{table:ILPInputs}
\end{table}

\vspace{3em}

\begin{table}[!h]
	\renewcommand{\arraystretch}{1.2}
	\centering
	\begin{tabular}{p{0.13\linewidth}p{0.77\linewidth}}
		\toprule
		\textbf{Variable} & \\
		\midrule
		$s_i$ & binary variable which equals 1 if item $i$ is placed in the bin and 0 otherwise \\
		$x_i, y_i, z_i$ & integer variables indicating coordinates of left-bottom-front corner of item $i$, given in respect to $x$, $y$ and $z$ \\
		$\alpha_{ij}$, $\beta_{ij}$, $\gamma_{ij}$ & binary variables which equal 1 if item $i$ overlaps with item $j$ and 0 otherwise in dimensions $x$, $y$ and $z$ respectively. These are given the name \textit{overlap indicators} in this paper \\
		$x^m_{ij}$, $y^m_{ij}$, $z^m_{ij}$ & binary variables which are used in indicator constraints to help determine the values of $\alpha_{ij}$, $\beta_{ij}$, $\gamma_{ij}$ respectively, where $m \in \{1, 2, 3, 4, 5, 6\}$. These are given the name \textit{intermediary variables} in this paper \\
		\bottomrule
	\end{tabular}
	\caption{Variables for the new ILP model}
	\label{table:ILPVariables}
\end{table}

\begin{figure}[h]
	\renewcommand{\arraystretch}{1.2}
	\centering
	\begin{mdframed}
		\begin{tabular}{p{0.12\linewidth}p{0.35\linewidth}p{0.4\linewidth}p{0.02\linewidth}}
			Maximise & $\mathlarger{\mathlarger{\sum}_{i=1}^{n}}s_i \cdot p_i \cdot q_i \cdot r_i$ & & (1) \\
			Subject to &&& \\
			& $x_i + p_i - M(1-s_i) \leq P$, & $\forall i \in I$, & (2) \\
			& $y_i + q_i - M(1-s_i) \leq Q$, & $\forall i \in I$, & (3) \\
			& $z_i + r_i - M(1-s_i) \leq R$, & $\forall i \in I$, & (4) \\
			&&&\\
			& $x_j - x_i + 1 \leq Mx^1_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (5) \\
			& $x_i - x_j + 1 \leq Mx^2_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (6) \\
			& $x_i + p_i - x_j \leq Mx^3_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (7) \\
			& $x_j + p_j - x_i \leq Mx^4_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (8) \\
			& $x^1_{ij} + x^3_{ij} \leq 1 + x^5_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (9) \\
			& $x^2_{ij} + x^4_{ij} \leq 1 + x^6_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (10) \\
			& $x^5_{ij} + x^6_{ij} \leq 2\alpha_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (11) \\
			\\
			& $y_j - y_i + 1 \leq My^1_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (12) \\
			& $y_i - y_j + 1 \leq My^2_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (13) \\
			& $y_i + q_i - y_j \leq My^3_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (14) \\
			& $y_j + q_j - y_i \leq My^4_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (15) \\
			& $y^1_{ij} + y^3_{ij} \leq 1 + y^5_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (16) \\
			& $y^2_{ij} + y^4_{ij} \leq 1 + y^6_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (17) \\
			& $y^5_{ij} + y^6_{ij} \leq 2\beta_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (18) \\
			\\
			& $x_j - x_i + 1 \leq Mx^1_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (19) \\
			& $x_i - x_j + 1 \leq Mx^2_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (20) \\
			& $x_i + p_i - x_j \leq Mx^3_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (21) \\
			& $x_j + p_j - x_i \leq Mx^4_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (22) \\
			& $x^1_{ij} + x^3_{ij} \leq 1 + x^5_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (23) \\
			& $x^2_{ij} + x^4_{ij} \leq 1 + x^6_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (24) \\
			& $x^5_{ij} + x^6_{ij} \leq 2\alpha_{ij}$, & $\forall i, j \in I$, $i \neq j$, & (25) \\
			\\
			& $\alpha_{ij} + \beta_{ij} + \gamma_{ij} \leq 4 - s_i - s_j$, & $\forall i, j \in I$, $i \neq j$, & (26) \\
			\\
			& $s_i \in \{0, 1\}$, & $\forall i \in I$, & (27) \\
			& $x_i$, $y_i$, $z_i \geq 0$, & $\forall i \in I$, & (28) \\
			& $\alpha_{ij}, \beta_{ij}, \gamma_{ij} \in \{0, 1\}$, & $\forall i,j \in I$, $i \neq j$, & (29) \\
			& $x^m_{ij}, y^m_{ij}, z^m_{ij} \in \{0, 1\}$, & $\forall i,j \in I$, $\forall m \in \{1, 2, 3, 4, 5, 6\}$, $i \neq j$ & (30) \\
		\end{tabular}
	\end{mdframed}
	\caption{Problem formulation for the new ILP model}
	\label{figure:ILPEquations}
\end{figure}
\FloatBarrier

From Figure \ref{figure:ILPEquations}, \textbf{Equation 1} defines the Objective Function of the problem; to maximise the occupied space within the bin.

\textbf{Equations 2-4} are bounds for item coordinates, ensuring no placed item can be located outside the boundaries of the bin. The use of $M$, referred to as \textit{Big} $M$ in combinatorial optimisation, is a method used regularly \cite{bigMLecture} to allow these constraints to only be applied when $s_i = 1$. Constraints of this type, which are only applied depending on the value of a decision variable, are often referred to as \textit{indicator constraints} \cite{bonami2015mathematical}.

\textbf{Equations 5-11, 12-18 and 19-25} are all indicator constraints; forcing the intermediary variables $\alpha_{ij}$, $\beta_{ij}$ and $\gamma_{ij}$ to indicate overlap in dimensions $x$, $y$ and $z$ respectively. To understand how these constraints correctly indicate overlap in a dimension, consider Equations 5-11 with dimension $x$. Looking at Figure \ref{CPModelConstraint4Figure} and Equation 5 from the CP model in Section \ref{CPModelDefinition}, Table \ref{table:InequalityMap} shows how the CP model inequalities map to the new ILP model variables.

\vspace{1.5em}

\begin{table}[!h]
	\renewcommand{\arraystretch}{1.2}
	\centering
	\begin{tabular}{rp{0.4\linewidth}}
		\toprule
		\textbf{Inequality} & \textbf{New ILP Model Variable} \\
		\midrule
		$x_i \leq x_j$ & is represented by $x^1_{ij}$ in Equation 5 \\
		$x_j \leq x_i$ & is represented by $x^2_{ij}$ in Equation 6 \\
		$x_j < x_i + p_i$ & is represented by $x^3_{ij}$ in Equation 7 \\
		$x_i < x_j + p_j$ & is represented by $x^4_{ij}$ in Equation 8 \\
		$x_i \leq x_j \wedge x_j < x_i + p_i$ & is represented by $x^5_{ij}$ in Equation 9 \\
		$x_j \leq x_i \wedge x_i < x_j + p_j$ & is represented by $x^6_{ij}$ in Equation 10 \\
		$A$ & is represented by $\alpha_{ij}$ in Equation 11 \\
		\bottomrule
	\end{tabular}
	\caption{Mapping of which new ILP model decision variables represent the inequalities from the CP model}
	\label{table:InequalityMap}
\end{table}

Therefore, it can be seen that the new ILP model presented here performs the same 'checks' in a logical sense as the CP model proposed in Section \ref{CPModelDefinition}. It is worth noting that, whilst the intermediary variables are forced to take the value of 1 when their condition is met, there is no method of forcing these variables to take the value of 0 when their condition is \textbf{not} met. Therefore, it is unknown whether a solver would set these variables to 0 or 1 if the corresponding inequalities in Table \ref{table:InequalityMap} were not met. These intermediary variables work in a constructive manner to set the overlap indicators $\alpha_{ij}$, $\beta_{ij}$ and $\gamma_{ij}$; the more intermediary variables are equal to 1, the higher likelihood that the overlap indicators are set to 1 as well. Because of this, the solver will prefer setting the intermediary variables to the value 0. This is due to, if an overlap is indicated in all dimensions, the solver will not be able to place that box, and hence value of the objective function is not increased. To summarise this, a solver will choose the best values for the intermediary variables which gets the objective function higher. This is explained well with a simple example in the blog post by Rubin \cite{liIndicatorsInMP}.

\textbf{Equation 26} enforces that every item $i$ cannot overlap in $x$, $y$ and $z$ with another item $j$, using the $\alpha$, $\beta$ and $\gamma$ overlap indicators.

Finally, \textbf{Equations 27-30} are domains for decision variables.

\vspace{1em}

To help demonstrate these constraints in practice, consider the solver determining the value of $\alpha_{ij}$ in dimension $x$ using the example instance from Table \ref{table:exerciseInstance}. Suppose the solver initialised the decision variables $s_1, s_2=1$, $x_1=0$ and $x_2=1$, where $i=1$ and $j=2$. Table \ref{table:ILPModelDemonstration} shows how the constraints would be evaluated.

\vspace{2em}

\begin{table}[h]
	\centering
	\renewcommand{\arraystretch}{1.2}
	\begin{tabular}{ccccc}
		\toprule
		\textbf{Equation} & \textbf{Inequality} & \textbf{Substitution} & \textbf{Holds} & \textbf{Variables Confined} \\
		\midrule
		$2$ & $x_i + p_i - M(1-s_i) \leq P$ & $0 + 4 - 5(1 - 1) \leq 4$ & Yes & None \\
		$2$ & $x_j + p_j - M(1-s_j) \leq P$ & $1 + 3 - 5(1 - 1) \leq 4$ & Yes & None \\
		$5$ & $x_j - x_i + 1 \leq Mx^1_{ij}$ & $1 - 0 + 1 \leq 5x^1_{12}$ & Yes & $x^1_{12}=1$ \\
		$6$ & $x_i - x_j + 1 \leq Mx^2_{ij}$ & $0 - 1 + 1 \leq 5x^2_{12}$ & Yes & None \\
		$7$ & $x_i + p_i - x_j \leq Mx^3_{ij}$ & $0 + 4 - 1 \leq 5x^3_{12}$ & Yes & $x^3_{12}=1$ \\
		$8$ & $x_j + p_j - x_i \leq Mx^4_{ij}$ & $1 + 3 - 0 \leq 5x^4_{12}$ & Yes & $x^4_{12}=1$ \\
		$9$ & $x^1_{ij} + x^3_{ij} \leq 1 + x^5_{ij}$ & $1 + 1 \leq 1 + x^5_{12}$ & Yes & $x^5_{12}=1$ \\
		$10$ & $x^2_{ij} + x^4_{ij} \leq 1 + x^6_{ij}$ & $0 + 1 \leq 1 + x^6_{12}$ & Yes & None \\
		$11$ & $x^5_{ij} + x^6_{ij} \leq 2\alpha_{ij}$ & $1 + 0 \leq 2\alpha_{12}$ & Yes & $\alpha_{12}=1$ \\
		\bottomrule
	\end{tabular}
	\caption{Indicates evaluation of $x$ dimension constraints for the Table \ref{table:exerciseInstance} instance. The 'Variables Confined' column shows whether a constraint has restricted a decision variable to a specific value}
	\label{table:ILPModelDemonstration}
\end{table}
\FloatBarrier

\vspace{1em}

Thus, in this instance $\alpha_{12}=1$, meaning an overlap was detected in dimension $x$. If then an overlap is detected in all three dimensions, i.e. $\alpha_{12}=1$, $\beta_{12}=1$ and $\gamma_{12}=1$, then the constraint in Equation 26 will not hold:
\begin{table}[h]
	\centering
	\begin{tabular}{c}
		$\alpha_{12} + \beta_{12} + \gamma_{12} \leq 4 - s_1 - s_2$ \\
		$1 + 1 + 1 \leq 4 - 1 - 1$ \\
		$\therefore$ \textbf{Does not hold}
	\end{tabular}
\end{table}
\FloatBarrier
For a walkthrough of the optimal solution for this instance, refer to Appendix \ref{ILPmodelAppendix}.

\vspace{2em}