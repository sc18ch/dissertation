The MIP model defined by Silva et al. \cite{silva2019} (referred to as the S model in this report), which is based upon the model proposed by Chen et al. \cite{chen1995} is to solve a three dimensional orthogonal bin packing problem, with rotations allowed. This formulation is more complex and solves a different problem than the ILP model proposed in this paper. It allows rotations, has multiple available bins with differing dimensions and it uses continuous variables for sizes and coordinates instead of integers. Therefore some adjustments were made to the S model so that it matched the problem defined in Section \ref{solving3DKP}, to ensure a fair comparison between each model.

The first of these is that just one bin should be considered ($n_1 = 1$), therefore any constraints relating to multiple containers will be ignored.

Secondly, rotations are not to be considered, therefore any related constraints are ignored and parallel indicator variables are adapted to constants with values:
\begin{center}
	$l_{xi}, w_{yi}, h_{zi} = 1$ \\
	$l_{yi}, l_{zi}, w_{xi}, w_{zi}, h_{xi}, h_{yi} = 0$
\end{center} 
For convenience, these indicator variables (which are now constants) will already be evaluated in the constraints of the adaptation.

Next, any continuous variables are to be adapted to integer variables.

Finally, the S model considers $x$, $y$ and $z$ dimensions in a different ordering to the model in section \ref{solving3DKP}; the S model will be changed to match this, visualised in Figure \ref{dimensionOrderingFigure}. Therefore when the S model refers to dimensions $x$, $y$ and $z$ to be 'length, width and height' respectively, the adaptation will refer to these dimensions as 'length, height and width'. This also applies to the parallel indicator variables discussed above - $l_{xi}$, $w_{yi}$ and $h_{zi}$ are now $l_{xi}$, $h_{yi}$ and $w_{zi}$.

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		
		\draw[thick,->,dashed] (0-5,0,0) -- (2-5,0,0) node [midway, anchor=north] {$x$};
		\draw[thick,->,dashed] (0-5,0,0) -- (0-5,2,0) node [midway, anchor=east] {$z$};
		\draw[thick,->,dashed] (0-5,0,0) -- (0-5,0,-3) node [anchor=north west] {$y$};
		\filldraw[fill=red] (0-5,0,0) circle (3pt);
		\draw (0.8-5,-1,0)node {S model};
		
		\draw[thick,->,dashed] (0,0,0) -- (2,0,0) node [midway, anchor=north] {$x$};
		\draw[thick,->,dashed] (0,0,0) -- (0,2,0) node [midway, anchor=east] {$y$};
		\draw[thick,->,dashed] (0,0,0) -- (0,0,-3) node [ anchor=north west] {$z$};
		\filldraw[fill=red] (0,0,0) circle (3pt);
		\draw (0.8,-1,0)node {Model in section \ref{solving3DKP}};
		
	\end{tikzpicture}
	\caption{Diagrams depicting the S model dimension ordering, and the model in Section \ref{solving3DKP}}
	\label{dimensionOrderingFigure}
\end{figure}

It is worth noting that two assumptions were forced to be made when adapting the S model. Firstly, a typo seems to be present in \textbf{Equation 12} of the published paper with the subscript of terms $p_i l_{wi}$ and $r_i h_{wi}$ in the inequality. The adaptation of the S model below will make the assumption that these terms are supposed to be $p_i l_{yi}$ and $r_i h_{yi}$. Secondly, the description in Table 3 for variables $x_i$, $y_i$ and $z_i$ defines these coordinates to be respective to the "front-left-bottom" corner of box $i$. Based on Fig. 3 and the equations in the published paper, this description does not match and should instead refer to the "left-front-bottom" corner. The adapted model will refer to $x$, $y$ and $z$ to be the left-bottom-front corner, due to the change in dimension ordering seen in Figure \ref{dimensionOrderingFigure}.

\begin{table}[h]
	\renewcommand{\arraystretch}{1.2}
	\centering
	\begin{tabular}{p{0.13\linewidth}p{0.50\linewidth}p{0.24\linewidth}}
		\toprule
		\textbf{Input} && \textbf{Domain} \\
		\midrule
		$n$ & the number of items & $n \in \mathbb{N}$, $n > 1$ \\
		$\mathcal{I}$ & the set of items to be packed in the bin & $\mathcal{I} = \{1, ..., n\}$ \\
		$p_i$, $q_i$, $r_i$ & $x$, $y$ and $z$ size of item $i$, respectively & $p_i, q_i, r_i \in \mathbb{Z}^{+}$, $\forall i \in \mathcal{I}$ \\
		$L, H, W$ & $x$, $y$ and $z$ size of the bin, respectively & $L, H, W \in \mathbb{Z}^{+}$ \\
		$M$ & large integer constant & $M = \max{(P, Q, R)} + 1$ \\
		\bottomrule
	\end{tabular}
	\caption{Inputs for the S model}
	\label{table:SilvaInputs}
\end{table}

\begin{table}[h]
	\renewcommand{\arraystretch}{1.2}
	\centering
	\begin{tabular}{p{0.22\linewidth}p{0.73\linewidth}}
		\toprule
		\textbf{Variable} & \\
		\midrule
		$s_i$ & binary variable which equals 1 if item $i$ is placed in the bin and 0 otherwise \\
		$x_i, y_i, z_i$ & integer variables indicating coordinates of left-bottom-front corner of item $i$, given in respect to $x$, $y$ and $z$ \\
		$a_{ik}, b_{ik}, c_{ik}, d_{ik}, e_{ik}, f_{ik}$ & indicator variables which represent the position of box $i$ in relation to box $k$ (to the left, right, below, above, behind and in front of $k$, respectively). See Figure \ref{relativePositionsFigure} for a visual explanation of these variables \\
		\bottomrule
	\end{tabular}
	\caption{Variables for the S model}
	\label{table:SilvaVariables}
\end{table}

\begin{figure}[h]
	\renewcommand{\arraystretch}{1.2}
	\centering
	\begin{mdframed}
		\begin{tabular}{p{0.12\linewidth}p{0.45\linewidth}p{0.3\linewidth}p{0.02\linewidth}}
			Minimise & $L \cdot W \cdot H - \mathlarger{\mathlarger{\sum}_{i=1}^{n}}s_i \cdot p_i \cdot q_i \cdot r_i$ & & (1) \\
			Subject to &&& \\
			& $x_i + p_i \leq L + (1 - s_i)M$, & $\forall i \in \mathcal{I}$, & (2) \\
			& $y_i + q_i \leq H + (1 - s_i)M$, & $\forall i \in \mathcal{I}$, & (3) \\
			& $z_i + r_i \leq W + (1 - s_i)M$, & $\forall i \in \mathcal{I}$, & (4) \\
			&&&\\
			& $x_i + p_i \leq x_k + (1 - a_{ik})M$, & $\forall i, k \in \mathcal{I}$, $i < k$, & (5) \\
			& $x_k + p_k \leq x_i + (1 - b_{ik})M$, & $\forall i, k \in \mathcal{I}$, $i < k$, & (6) \\
			& $y_i + q_i \leq y_k + (1 - c_{ik})M$, & $\forall i, k \in \mathcal{I}$, $i < k$, & (7) \\
			& $y_k + q_k \leq y_i + (1 - d_{ik})M$, & $\forall i, k \in \mathcal{I}$, $i < k$, & (8) \\
			& $z_i + r_i \leq z_k + (1 - e_{ik})M$, & $\forall i, k \in \mathcal{I}$, $i < k$, & (9) \\
			& $z_k + r_k \leq z_i + (1 - f_{ik})M$, & $\forall i, k \in \mathcal{I}$, $i < k$, & (10) \\
			\\
			& $a_{ik} + b_{ik} + c_{ik} + d_{ik} + e_{ik} + f_{ik} \geq s_i + s_k - 1$, & $\forall i, k \in \mathcal{I}$, $i < k$, & (11) \\
			\\
			& $s_i \in \{0, 1\}$, & $\forall i \in I$, & (12) \\
			& $x_i$, $y_i$, $z_i \geq 0$, & $\forall i \in I$, & (13) \\
			& $a_{ik}, b_{ik}, c_{ik}, d_{ik}, e_{ik}, f_{ik} \in \{0, 1\}$, & $\forall i,k \in I$, $i < k$ & (14) \\
		\end{tabular}
	\end{mdframed}
	\caption{Problem formulation for the S model}
	\label{figure:SilvaEquations}
\end{figure}
\FloatBarrier

\textbf{Equation 1} defines the objective function of the S model. It is formulated as a minimisation problem for wasted space inside the bin, which is essentially the same goal as maximising the occupied space within the bin from the previously defined models.

\textbf{Equations 2-4} are bounds for item coordinates, ensuring no placed item can be located outside the boundaries of the bin. These are equivalent constraints to the ILP model defined in Section \ref{ILPModelDefinition}.

\textbf{Equations 5-11} are constraints to ensure no overlapping between boxes. This is achieved using variables to indicate where box $i$ is in relation to box $k$. For example, if $a_{ik}$ takes the value of $1$, the inequality $x_i + p_i \leq x_k$ in Equation 5 will be enforced - thus box $i$ \textbf{must} be to the left of box $k$. Figure \ref{relativePositionsFigure} demonstrates this visually for Equation 5; in this instance, $a_{ik}=1$ and $x_i+p_i=x_k$.

\begin{figure}[!h]
	\centering
	\begin{tikzpicture}
		
		\draw[black, fill=cyan!60] (0,0,0) -- (0,1,0) -- (2.5,1,0) -- (2.5,0,0) -- cycle;
		\draw[black, fill=blue!10] (0,1,0) -- (0,1,-2) -- (2.5,1,-2) -- (2.5,1,0) -- cycle;
		
		\draw[black, fill=orange!60] (0+2.5,0,0) -- (0+2.5,1,0) -- (2.5+2.5,1,0) -- (2.5+2.5,0,0) -- cycle;
		\draw[black, fill=orange!40] (2.5+2.5,0,0) -- (2.5+2.5,1,0) -- (2.5+2.5,1,-2) -- (2.5+2.5,0,-2) -- cycle;
		\draw[black, fill=orange!20] (0+2.5,1,0) -- (0+2.5,1,-2) -- (2.5+2.5,1,-2) -- (2.5+2.5,1,0) -- cycle;
		\draw (2.5/2, 1/2, 0) node{$i$};
		\draw (2.5 + 2.5/2, 1/2, 0) node{$k$};
		
		\draw[thick,<->] (0,1.2,0) -- (2.5,1.2,0) node [midway, anchor=south] {$p_i$};
		\node[label=below:$x_i$] at (0, 0, 0) {$\bullet$};
		\node[label=below:$x_k$] at (2.5, 0, 0) {$\bullet$};
		\draw (2.5,-1.5,0) node {Instance where $a_{ik}=1$};
		
		\draw[thick,->] (0-7 + 5/4,0,-2/2) -- (0-7 + 5/4,-1.2,-2/2) node [right, anchor=west] {$d_{ik}$};
		\draw[thick,->] (0-7,1/2,-2/2) -- (0-7 - 1.2,1/2,-2/2) node [right, anchor=south] {$b_{ik}$};
		\draw[thick,->] (0-7 + 5/4,1/2,-2) -- (0-7 + 5/4,1/2,-2 - 2) node [right, anchor=south east] {$f_{ik}$};
		
		\draw[black, fill=cyan!60] (0-7,0,0) -- (0-7,1,0) -- (2.5-7,1,0) -- (2.5-7,0,0) -- cycle;
		\draw[black, fill=cyan!20] (2.5-7,0,0) -- (2.5-7,1,0) -- (2.5-7,1,-2) -- (2.5-7,0,-2) -- cycle;
		\draw[black, fill=blue!10] (0-7,1,0) -- (0-7,1,-2) -- (2.5-7,1,-2) -- (2.5-7,1,0) -- cycle;
		
		\draw[thick,->] (0-7 + 5/2,1/2,-2/2) -- (0-7 + 5/2 + 1,1/2,-2/2) node [right, anchor=south] {$a_{ik}$};
		\draw[thick,->] (0-7 + 5/4,1,-2/2) -- (0-7 + 5/4,1.8,-2/2) node [right, anchor=east] {$c_{ik}$};
		\draw[thick,->] (0-7 + 5/4,1/2,0) -- (0-7 + 5/4,1/2,1.5) node [right, anchor=south east] {$e_{ik}$};
		\draw (-5.5,-1.5,0) node {Relative positions of $k$ from $i$};
		\draw (-2.5/2 - 5 + 1, 1/2, 0) node{$i$};
		
		
	\end{tikzpicture}
	\caption{How the relative position of $k$ from $i$ is shown through the indication variables}
	\label{relativePositionsFigure}
\end{figure}

With \textbf{Equation 11}, it is ensured that at least one of the relative position indicator variables are equal to $1$ if both item $i$ and item $k$ are placed in the container, thus preventing overlapping between these two items.

Finally, \textbf{Equations 12-14} are domains for decision variables.

To see these constraints in practice, refer to Appendix \ref{SilvamodelAppendix} for a walkthrough of the example instance defined in Table \ref{table:exerciseInstance}.