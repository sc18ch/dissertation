package io;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;
import java.util.ArrayList;
import components.*;

public class DataFile {

    public static void saveData(File file, PackingInstance problem) throws Exception {
        
        String fileContents = "";
        
        // Format number of boxes
        fileContents += "n = " + problem.getBoxes().getBoxes().size() + ";\n";

        // Format item sizes
        fileContents += "itemSizes = [|";
        for (BoxItem box : problem.getBoxes().getBoxes()) {
            fileContents += " " + ((int) box.getSizeX()) + ", ";
            fileContents += ((int) box.getSizeY()) + ", ";
            fileContents += ((int) box.getSizeZ()) + ",|";
        }
        fileContents += "];\n";

        // Format container size
        fileContents += "bin = [" + ((int) problem.getContainer().getSizeX()) + ", ";
        fileContents += ((int) problem.getContainer().getSizeY()) + ", ";
        fileContents += ((int) problem.getContainer().getSizeZ()) + "];";

        // Write to file
        FileWriter writer = new FileWriter(file.getAbsolutePath() + ".dzn");
        writer.write(fileContents);
        writer.close();
    }
    
    public static PackingInstance loadProblemFromDataFile(File file) throws Exception {

        PackingInstance bpp = new PackingInstance();

        // Read file
        String fileContents = "";
        Scanner sc = new Scanner(file);
        while (sc.hasNextLine()) {
            fileContents += sc.nextLine() + "\n";
        }
        sc.close();

        // Split file by line
        String fileElements[] = fileContents.split("\n");

        // Get number of boxes
        int boxNumber = Integer.valueOf(fileElements[0].split("=")[1].replace(";", "").strip());
        //System.out.println("Box Number = " + boxNumber);

        // Get box sizes
        ArrayList<ArrayList<Integer>> boxSizes = new ArrayList<ArrayList<Integer>>();
        String tempBoxSizes[] = fileElements[1].split("\\[")[1].split("\\]")[0].split("\\|");

        for (int i = 0; i < boxNumber; i++) {
            boxSizes.add(new ArrayList<Integer>());
            for (int j = 0; j < 3; j++) {
                boxSizes.get(i).add(Integer.valueOf(tempBoxSizes[i+1].split(",")[j].strip()));
            }
        }
        //System.out.println("Box Sizes = " + boxSizes);

        // Get container size
        ArrayList<Integer> containerSize = new ArrayList<Integer>();
        String tempContainerSizes[] = fileElements[2].split("\\[")[1].split("\\]")[0].split(",");

        for (int i = 0; i < 3; i++) {
            containerSize.add(Integer.valueOf(tempContainerSizes[i].strip()));
        }
        //System.out.println("Container Size = " + containerSize);

        // Set up BPP
        bpp.setContainer(new Container(containerSize.get(0), containerSize.get(1), containerSize.get(2)));
        bpp.setBoxes(new BoxItemList(boxSizes));
        
        return bpp;
    }
}
