package io;

import components.*;
import scene.MainScene;

import java.io.File;
import java.util.Scanner;
import java.util.ArrayList;
import org.json.*;

public class LPOutputFile {

    private final File file;
    private final MainScene scene3D;
    private String fileContents = null;


    public LPOutputFile(File file, MainScene scene3D) {
        this.file = file;
        this.scene3D = scene3D;
    }

    public void readContents() throws Exception {
        Scanner sc = new Scanner(file);
        fileContents = "";
        while (sc.hasNextLine()) {
            fileContents += sc.nextLine() + "\n";
        }
        sc.close();
    }

    public boolean isLogFile() {
        return this.getExtension().equals(".log");
    }

    public String getExtension() {
        return file.getName().substring(file.getName().length()-4);
    }

    public void loadBoxPositions() throws Exception {

        // Read file contents if not done before
        if (fileContents == null) {
            try {
                readContents();
            }
            catch (Exception e) {
                throw e;
            }
        }

        JSONObject json = new JSONObject(fileContents);

        ArrayList<Boolean> placed = this.getPlacedArray(json.getJSONArray("placed"));
        ArrayList<ArrayList<Integer>> coordinates = this.getCoordinateArray(json.getJSONArray("itemCoordinates"));

        PackingInstance bpp = scene3D.getProblem();

        // Place box items
        for (int i = 0; i < coordinates.size(); i++) {
            
            BoxItem box = bpp.getBoxes().getBoxAt(i);
            box.setName("Box " + (i+1));

            if (placed.get(i)) {

                double x = coordinates.get(i).get(0) + box.getSizeX()/2;
                double y = -(coordinates.get(i).get(1) + box.getSizeY()/2);
                double z = coordinates.get(i).get(2) + box.getSizeZ()/2;

                box.placeBoxAt(x, y, z);
            }
            else {
                System.out.println("Box " + (i+1) + " was not placed!");
            }
        }
    }

    private ArrayList<ArrayList<Integer>> getCoordinateArray(JSONArray arr) {

        ArrayList<ArrayList<Integer>> coordinates = new ArrayList<ArrayList<Integer>>();

        for (int i = 0; i < arr.length(); i++) {

            JSONArray innerArray = arr.getJSONArray(i);
            coordinates.add(new ArrayList<Integer>());

            for (int j = 0; j < innerArray.length(); j++) {
                coordinates.get(i).add(innerArray.getInt(j));
            }
        }

        return coordinates;
    }

    /**
     * Parses 'placed' JSON Array to ArrayList 
     * @param arr
     * @return array parsed from JSON containing indicator for if box is placed or not
     */
    private ArrayList<Boolean> getPlacedArray(JSONArray arr) {

        ArrayList<Boolean> placed = new ArrayList<Boolean>();

        for (int i = 0; i < arr.length(); i++) {
            if (arr.getInt(i) == 1) {
                placed.add(true);
            }
            else {
                placed.add(false);
            }
        }

        return placed;
    }

    public File getFile() {
        return this.file;
    }
}
