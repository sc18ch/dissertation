package scene;

import javafx.scene.shape.Box;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.geometry.Point3D;

public class BinVisual extends Box {

    private final PhongMaterial material;
    private final double itemToContainerOffset;

    private Point3D centerPoint; 
    private double sizeX;
    private double sizeY;
    private double sizeZ;

    public BinVisual(double x, double y, double z, Color c, double itemToContainerOffset) {
        super(x+itemToContainerOffset, y+itemToContainerOffset, z+itemToContainerOffset);

        this.sizeX = x;
        this.sizeY = y;
        this.sizeZ = z;
        this.itemToContainerOffset = itemToContainerOffset;
        this.centerPoint = new Point3D(x/2, -y/2, z/2);
        this.material = new PhongMaterial(c);
        this.setMaterial(material);

        this.setTranslateX(centerPoint.getX());
        this.setTranslateY(centerPoint.getY());
        this.setTranslateZ(centerPoint.getZ());
    }

    /**
     * @return center point of the Bin
     */
    public Point3D getCenter() {
        return centerPoint;
    }
}