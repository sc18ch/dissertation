package scene;

import javafx.scene.shape.Line;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.transform.Rotate;

public class Axis extends Pane {

    public Line xLine;
    public Line xLineNegative;
    public Line yLine;
    public Line yLineNegative;
    public Line zLine;
    public Line zLineNegative;

    public Axis(double size, double strokeWidth) {

        xLine = new Line(0, 0, size, 0);
        xLine.setStroke(Color.RED);
        xLine.setFill(Color.RED);
        xLine.setStrokeWidth(strokeWidth);

        xLineNegative = new Line(-size, 0, 0, 0);
        xLineNegative.setStroke(Color.MAGENTA);
        xLineNegative.setFill(Color.MAGENTA);
        xLineNegative.setStrokeWidth(strokeWidth);

        yLine = new Line(0, 0, 0, size);
        yLine.setStroke(Color.GREEN);
        yLine.setFill(Color.GREEN);
        yLine.setStrokeWidth(strokeWidth);

        yLineNegative = new Line(0, -size, 0, 0);
        yLineNegative.setStroke(Color.YELLOW);
        yLineNegative.setFill(Color.YELLOW);
        yLineNegative.setStrokeWidth(strokeWidth);

        zLine = new Line(-size/2, 0, size/2, 0);
        zLine.setStroke(Color.BLUE);
        zLine.setFill(Color.BLUE);
        zLine.setStrokeWidth(strokeWidth);
        zLine.setRotationAxis(Rotate.Y_AXIS);
        zLine.setRotate(90);
        zLine.setTranslateZ(size/2);

        zLineNegative = new Line(-size/2, 0, size/2, 0);
        zLineNegative.setStroke(Color.CYAN);
        zLineNegative.setFill(Color.CYAN);
        zLineNegative.setStrokeWidth(strokeWidth);
        zLineNegative.setRotationAxis(Rotate.Y_AXIS);
        zLineNegative.setRotate(90);
        zLineNegative.setTranslateZ(-size/2);

        getChildren().add(xLine);
        getChildren().add(yLine);
        getChildren().add(zLine);
        getChildren().add(xLineNegative);
        getChildren().add(yLineNegative);
        getChildren().add(zLineNegative);
    }

}