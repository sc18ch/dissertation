package scene;

import gui.OptionsBarGUI;
import scene.Axis;
import scene.BinVisual;
import components.*;
import io.LPOutputFile;

import java.util.Random;

import javafx.stage.Stage;
import javafx.scene.SubScene;
import javafx.scene.Group;
import javafx.scene.SceneAntialiasing;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.transform.Rotate;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.KeyEvent;
import javafx.event.EventHandler;
import javafx.scene.PerspectiveCamera;
import javafx.scene.control.MenuBar;
import javafx.geometry.Point3D;

public class MainScene extends SubScene {

    private static final Random RANDOM = new Random();
    private static final double MOUSE_SENSITIVITY = 0.2;
    private static final Color BIN_COLOUR = Color.rgb(40, 80, 80, 0.15);
    private static final double ITEM_TO_CONTAINER_OFFSET = 10;

    private final Stage primaryStage;
    private final double width;
    private final double height;
    private final PerspectiveCamera sceneCamera = new PerspectiveCamera(true);
    private final Group sceneRoot;
    private final Group componentsGroup;
    private final Rotate sceneRotateX = new Rotate(0, Rotate.X_AXIS);
    private final Rotate sceneRotateY = new Rotate(0, Rotate.Y_AXIS);

    private double mousePosX, mousePosY;
    private double mouseOldPosX, mouseOldPosY;
    private PackingInstance bppProblem = new PackingInstance();
    private OptionsBarGUI optionsBar;
    private BinVisual binComponent;
    private Axis sceneAxis = new Axis(1000, 3);
    
    public MainScene(Stage primaryStage, Group sceneRoot, double width, double height) {
        super(sceneRoot, width, height, true, SceneAntialiasing.BALANCED);

        this.primaryStage = primaryStage;
        this.sceneRoot = sceneRoot;
        this.width = width;
        this.height = height;
        this.componentsGroup = new Group();
        this.setFill(Color.LIGHTGRAY);
        sceneAxis.setVisible(false);

        componentsGroup.getTransforms().addAll(sceneRotateX, sceneRotateY);
        sceneRoot.getChildren().add(componentsGroup);

        setupCamera();
        bindKeyboardControls();
        bindMouseControls();

        testScene();
    }

    /**
     * Generates a test scene, used to test the box and container scene creation method
     */
    private void testScene() {

        int layers = 3;
        Container bin = new Container(900, 400, 600);
        BoxItemList boxes = new BoxItemList();
        boxes.generateUniformBoxes(6*layers, 150, 100, 600);

        double xSpacing = 0;
        double yValue = -boxes.getBoxAt(0).getSizeY()/2;
        int boxCount = 0;

        for (BoxItem box : boxes.getBoxes()) {

            if (boxCount == boxes.getBoxes().size() / layers) {
                yValue -= box.getSizeY();
                xSpacing = 0;
                boxCount = 0;
            }

            box.placeBoxAt(xSpacing + box.getSizeX()/2, yValue, box.getSizeZ()/2);
            xSpacing += box.getSizeX();

            boxCount++;
        }

        //System.out.println("\n<----- TEST SCENE ---->\n" + bin + "\n" + boxes);

        this.bppProblem.setContainer(bin);
        this.bppProblem.setBoxes(boxes);

        loadPackingScene();
    }

    /**
     * Generates scene with current bin packing problem
     */
    public void loadPackingScene() {

        Container bin = this.bppProblem.getContainer();
        BoxItemList boxes = this.bppProblem.getBoxes();

        componentsGroup.getChildren().clear();

        binComponent = new BinVisual(bin.getSizeX(), bin.getSizeY(), bin.getSizeZ(), BIN_COLOUR, ITEM_TO_CONTAINER_OFFSET);
        setScenePivot(binComponent.getCenter());

        for (BoxItem box : boxes.getBoxes()) {

            // Only place boxes with coordinates
            if (box.isPlaced()) {
                box.setBoxComponent(new Box(box.getSizeX(), box.getSizeY(), box.getSizeZ()));
                box.setMaterial(new PhongMaterial(getRandomColour(1)));

                componentsGroup.getChildren().addAll(box.getBoxComponent());
            }  
        }

        componentsGroup.getChildren().addAll(binComponent);
        componentsGroup.getChildren().addAll(sceneAxis);
        sceneCamera.setTranslateX(binComponent.getCenter().getX());
        sceneCamera.setTranslateY(1.5 * binComponent.getCenter().getY() - 1000);

        if (this.optionsBar != null) {
            this.optionsBar.generateButtons();
        }
    }

    /**
     * Sets up the pivot point for the scene to be rotatable
     * @param p pivot point to rotate scene around
     */
    private void setScenePivot(Point3D p) {
        sceneRotateX.setPivotX(p.getX());
        sceneRotateX.setPivotY(p.getY());
        sceneRotateX.setPivotZ(p.getZ());

        sceneRotateY.setPivotX(p.getX());
        sceneRotateY.setPivotY(p.getY());
        sceneRotateY.setPivotZ(p.getZ());
    }

    /**
     * Sets up camera for the scene
     */
    private void setupCamera() {
        sceneCamera.setTranslateX(300);
        sceneCamera.setTranslateY(-800);
        sceneCamera.setTranslateZ(-2000);
        sceneCamera.setFarClip(10000);
        sceneCamera.getTransforms().addAll(new Rotate(-30, Rotate.X_AXIS));
        this.setCamera(sceneCamera);
    }

    /**
     * Sets up and binds keyboard controls
     */
    private void bindKeyboardControls() {
        primaryStage.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            switch (event.getCode()) {
                case W:
                    sceneCamera.translateZProperty().set(sceneCamera.getTranslateZ() + 100);
                    break;
                case S:
                    sceneCamera.translateZProperty().set(sceneCamera.getTranslateZ() - 100);
                    break;
                case D:
                    sceneCamera.translateXProperty().set(sceneCamera.getTranslateX() + 100);
                    break;
                case A:
                    sceneCamera.translateXProperty().set(sceneCamera.getTranslateX() - 100);
                    break;
            }
        });
    }

    /**
     * Sets up and binds mouse controls
     */
    private void bindMouseControls() {
        this.setOnMousePressed(event -> {
            mouseOldPosX = event.getSceneX();
            mouseOldPosY = event.getSceneY();
        });
        
        this.setOnMouseDragged(event -> {

            mousePosX = event.getSceneX();
            mousePosY = event.getSceneY();

            if (event.getButton() == MouseButton.MIDDLE) {
                sceneCamera.setTranslateX(sceneCamera.getTranslateX() - (mousePosX - mouseOldPosX) * MOUSE_SENSITIVITY*2.5);
                sceneCamera.setTranslateY(sceneCamera.getTranslateY() - (mousePosY - mouseOldPosY) * MOUSE_SENSITIVITY*2.5);
            }

            else if (event.getButton() == MouseButton.SECONDARY) {
                sceneRotateX.setAngle(sceneRotateX.getAngle() - (mousePosY - mouseOldPosY) * MOUSE_SENSITIVITY);
                sceneRotateY.setAngle(sceneRotateY.getAngle() - (mousePosX - mouseOldPosX) * MOUSE_SENSITIVITY);
            }

            mouseOldPosX = mousePosX;
            mouseOldPosY = mousePosY;

        });
    }

    /**
     * Sets the scene's options bar menu
     * @param optionsBar
     */
    public void setOptionsBar(OptionsBarGUI optionsBar) {
        this.optionsBar = optionsBar;
    }

    /**
     * Gets the scene axis object associated with scene
     * @return Axis object
     */
    public Axis getSceneAxis() {
        return sceneAxis;
    }

    /**
     * Get the current BinVisual object associated with scene
     * @return BinVisual object
     */
    public BinVisual getBinComponent() {
        return binComponent;
    }

    /**
     * Gets the current bin packing problem
     * @return the current bpp problem
     */
    public PackingInstance getProblem() {
        return this.bppProblem;
    }

    /**
     * Sets the current bin packing problem and generates a scene
     * @param bppProblem
     */
    public void setProblem(PackingInstance bppProblem) {
        this.bppProblem = bppProblem;
        this.loadPackingScene();
    }

    /**
     * Loads a given LP output file into a scene
     * @param lpFile file to load
     */
    public void loadSceneFromLP(LPOutputFile lpFile) throws Exception {

        // Check for correct file extension
        if (!lpFile.isLogFile()) {
            throw new Exception("File extension invalid");
        }

        try {
            lpFile.loadBoxPositions();
            System.out.println("<----- LOADED BOXES FROM FILE ----->\n" + this.bppProblem.getBoxes());
            this.loadPackingScene();
        }
        catch (Exception e) {
            System.out.print("ERROR LOADING BOXES --> ");
            System.out.println(e);
            throw e;
        }    
    }

    /**
     * Returns a random JavaFX Color
     * @return random colour
     */
    public static Color getRandomColour(double alpha) {
        return Color.rgb(RANDOM.nextInt(255), RANDOM.nextInt(255), RANDOM.nextInt(255), alpha);
    }
}
