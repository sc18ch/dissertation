package gui;

import scene.MainScene;
import io.*;

import java.io.File;
import java.nio.file.Paths;
import java.util.Optional;

import components.PackingInstance;
import javafx.application.Application;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class ApplicationGUI extends Application {

    private final String currentDirectory = Paths.get(".").toAbsolutePath().normalize().toString();
    private final MenuBar menuBar = new MenuBar();

    private final Menu fileMenu = new Menu("File");
    private final Menu instanceMenu = new Menu("Instance");
    private final Menu sceneMenu = new Menu("Scene");
    private final MenuItem saveDataMenuItem = new MenuItem("Save to Data File");
    private final MenuItem openDataMenuItem = new MenuItem("Load from Data File");
    private final MenuItem openLPMenuItem = new MenuItem("Open Output Log File");
    private final MenuItem generateInstanceMenuItem = new MenuItem("Generate New Instance");
    private final MenuItem showBinMenuItem = new MenuItem("Toggle Bin Visibility");
    private final MenuItem showAxisMenuItem = new MenuItem("Toggle Axis Visibility");
    private final FileChooser fileChooser = new FileChooser();
    private final ExtensionFilter dataExtensionFilter = new ExtensionFilter("DZN file (.dzn)", "*.dzn");
    private final ExtensionFilter outputExtensionFilter = new ExtensionFilter("LOG file (.log)", "*.log");

    private final BorderPane globalRoot = new BorderPane();
    private final Group scene3DRoot = new Group();

    private Scene globalScene;
    private Stage primaryStage;
    private MainScene scene3D;
    private OptionsBarGUI optionsBar;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        setupScene();
    }

    private void setupScene() {

        // Setting up menus
        setupMenus();

        // Creating global scene and adding menu to it
        globalScene = new Scene(globalRoot, 1600, 900, true);
        globalRoot.setTop(menuBar);

        // Creating sub scene and adding it to the global scene
        scene3D = new MainScene(primaryStage, scene3DRoot, 1400, 800);
        globalRoot.setLeft(scene3D);

        // Configuring options area
        optionsBar = new OptionsBarGUI(this, globalRoot, scene3D);
        scene3D.setOptionsBar(optionsBar);
        globalRoot.setRight(optionsBar);

        // Making sub scene resize on window resize
        scene3D.heightProperty().bind(globalRoot.heightProperty());
        scene3D.widthProperty().bind(globalRoot.widthProperty().multiply(0.85));
        optionsBar.prefWidthProperty().bind(globalRoot.widthProperty().multiply(0.15));

        // Adding CSS style sheet
        globalScene.getStylesheets().add(getClass().getResource("/stylesheet.css").toExternalForm());

        // Setting up the primary stage
        primaryStage.setTitle("3D KP Visualisation");
        primaryStage.setResizable(true);
        primaryStage.setScene(globalScene);
        primaryStage.setMinWidth(1400);
        primaryStage.setMinHeight(800);
        primaryStage.show();

        
    }

    private void setupMenus() {

        // Adding menu items to menus
        //fileMenu.getItems().addAll(saveDataMenuItem, openDataMenuItem, openLPMenuItem);
        fileMenu.getItems().addAll(saveDataMenuItem, openLPMenuItem);
        instanceMenu.getItems().addAll(generateInstanceMenuItem);
        sceneMenu.getItems().addAll(showBinMenuItem, showAxisMenuItem);

        // Adding menus to menu bar
        menuBar.getMenus().addAll(fileMenu, instanceMenu, sceneMenu);

        // Setting up event handlers on menu item clicks
        setupFileMenuItemActions();
        setupInstanceMenuItemActions();
        setupSceneMenuItemActions();
    }

    private void setupSceneMenuItemActions() {

        showBinMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(final ActionEvent e) {
                if (scene3D.getBinComponent().isVisible()) {
                    scene3D.getBinComponent().setVisible(false);
                }
                else {
                    scene3D.getBinComponent().setVisible(true);
                }
            }
        });

        showAxisMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(final ActionEvent e) {
                if (scene3D.getSceneAxis().isVisible()) {
                    scene3D.getSceneAxis().setVisible(false);
                }
                else {
                    scene3D.getSceneAxis().setVisible(true);
                }
            }
        });
    }

    private void setupInstanceMenuItemActions() {

        generateInstanceMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(final ActionEvent e) {
                newInstance();
            }
        });
    }

    private int checkIntegerInput(Optional<String> result, int min, int max, String errorMessage) {

        int value;

        if (result.isPresent()) {
            value = convertAndValidateStringToInt(result.get(), min, max);

            if (value != -1) {
                return value;
            }
            else {
                displayErrorAlert(errorMessage);
                return -1;
            }
        }
        else {
            return -1;
        }
    }

    private void newInstance() {

        int numberOfBoxes;
        int minSize;
        int maxSize;
        int binX;
        int binY;
        int binZ;
        TextInputDialog dialogBox = new TextInputDialog();

        dialogBox.setTitle("Input a parameter");
        dialogBox.setHeaderText("Enter the number of boxes to generate");

        Optional<String> result = dialogBox.showAndWait(); 

        numberOfBoxes = checkIntegerInput(result, 1, 50, "Invalid number of boxes input! Input must be an integer and between 1-50");

        if (numberOfBoxes != -1) {
            dialogBox.getEditor().clear();
            dialogBox.setHeaderText("Enter the minimum size for box x, y and z dimensions");
            result = dialogBox.showAndWait();
            minSize = checkIntegerInput(result, 20, 800, "Invalid minimum size for boxes! Input must be an integer and between 20-800"); 

            if (minSize != -1) {
                dialogBox.getEditor().clear();
                dialogBox.setHeaderText("Enter the maximum size for box x, y and z dimensions");
                result = dialogBox.showAndWait();
                maxSize = checkIntegerInput(result, 40, 1000, "Invalid maximum size for boxes! Input must be an integer and between 40-1000");

                if (maxSize != -1) {
                    dialogBox.getEditor().clear();
                    dialogBox.setHeaderText("Enter the X size for the container");
                    result = dialogBox.showAndWait();
                    binX = checkIntegerInput(result, 100, 3000, "Invalid size for the container! Input must be an integer and between 100-3000");

                    if (binX != 1) {
                        dialogBox.getEditor().clear();
                        dialogBox.setHeaderText("Enter the Y size for the container");
                        result = dialogBox.showAndWait();
                        binY = checkIntegerInput(result, 100, 3000, "Invalid size for the container! Input must be an integer and between 100-3000");
                        
                        if (binY != 1) {
                            dialogBox.getEditor().clear();
                            dialogBox.setHeaderText("Enter the Z size for the container");
                            result = dialogBox.showAndWait();
                            binZ = checkIntegerInput(result, 100, 3000, "Invalid size for the container! Input must be an integer and between 100-3000");
                            
                            if (binY != 1) {
                                generateNewInstance(numberOfBoxes, minSize, maxSize, binX, binY, binZ);
                            }
                        }
                    }
                }
            }
        }
    }

    private void generateNewInstance(int numberOfBoxes, int minSize, int maxSize, int binX, int binY, int binZ) {
        try {
            scene3D.setProblem(PackingInstance.generateRandomInstance(numberOfBoxes, minSize, maxSize, binX, binY, binZ));
            displayInformationAlert("Instance generated successfully!\n\nNOTE: Try saving this instance to a data file to be used in a solver");
        }
        catch (Exception e) {
            displayErrorAlert("Instance could not be generated:\n\n" + e);
        }
    }

    private int convertAndValidateStringToInt(String value, int min, int max) {

        try {
            int convertedValue = Integer.parseInt(value);

            if (convertedValue < min || convertedValue > max) {
                return -1;
            }
            return convertedValue;
        }
        catch (NumberFormatException e) {
            return -1;
        }
    }

    private void setupFileMenuItemActions() {

        fileChooser.setInitialDirectory(new File(currentDirectory + "/solver"));
        fileChooser.getExtensionFilters().addAll(dataExtensionFilter, outputExtensionFilter);

        saveDataMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(final ActionEvent e) {
                fileChooser.setSelectedExtensionFilter(dataExtensionFilter);
                fileChooser.setTitle("Choose a location to save problem configuration to");
                File file = fileChooser.showSaveDialog(primaryStage);
                if (file != null) {
                    saveDataFile(file);
                }
            }
        });

        openDataMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(final ActionEvent e) {
                fileChooser.setSelectedExtensionFilter(dataExtensionFilter);
                fileChooser.setTitle("Choose a file to load problem configuration from");
                File file = fileChooser.showOpenDialog(primaryStage);
                if (file != null) {
                    openDataFile(file);
                }
            }
        });

        openLPMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(final ActionEvent e) {

                fileChooser.setSelectedExtensionFilter(dataExtensionFilter);

                // Load data file
                fileChooser.setTitle("Choose a file to load problem configuration from");
                File file = fileChooser.showOpenDialog(primaryStage);
                if (file != null) {
                    openDataFile(file);

                    fileChooser.setSelectedExtensionFilter(outputExtensionFilter);

                    // Load LP Output
                    fileChooser.setTitle("Choose a file to load LP Output from");
                    file = fileChooser.showOpenDialog(primaryStage);
                    if (file != null) {
                        openLPFile(new LPOutputFile(file, scene3D));
                    } 
                }
            }
        });
    }

    private void displayInformationAlert(String message) {
        Alert alertMessage = new Alert(Alert.AlertType.INFORMATION);
        alertMessage.setContentText(message);
        alertMessage.showAndWait();
    }

    private void displayErrorAlert(String message) {
        Alert alertError = new Alert(Alert.AlertType.ERROR);
        alertError.setContentText(message);
        alertError.showAndWait();
    }

    private void saveDataFile(File file) {

        try {
            DataFile.saveData(file, scene3D.getProblem());
            displayInformationAlert("File '" + file.getName() + ".dzn' saved successfully!");
        }
        catch (Exception e) {
            displayErrorAlert("File '" + file.getName() + ".dzn' could not be saved:\n\n" + e);
        }
    }

    private void openDataFile(File file) {

        try {
            scene3D.setProblem(DataFile.loadProblemFromDataFile(file));
            displayInformationAlert("File '" + file.getName() + "' loaded successfully!");
        }
        catch (Exception e) {
            displayErrorAlert("File '" + file.getName() + "' could not be loaded:\n\n" + e);
        }
    }

    private void openLPFile(LPOutputFile lpFile) {

        try {
            scene3D.loadSceneFromLP(lpFile);
            displayInformationAlert("File '" + lpFile.getFile().getName() + "' loaded successfully!");
        }
        catch (Exception e) {
            displayErrorAlert("File '" + lpFile.getFile().getName() + "' could not be loaded:\n\n" + e);
        }
    }
}