package gui;

import scene.MainScene;
import components.*;

import java.util.ArrayList;

import javafx.scene.layout.BorderPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.control.ScrollPane;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.scene.paint.Color;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.layout.Priority;

public class OptionsBarGUI extends BorderPane {

    private final ApplicationGUI app;
    private final BorderPane globalRoot;
    private final MainScene mainScene;
    private final VBox buttonLayout = new VBox();
    private final ScrollPane scrollPane = new ScrollPane();
    private final ArrayList<ToggleButton> buttons = new ArrayList<ToggleButton>();
    private final VBox statsLayout = new VBox();
    
    public OptionsBarGUI(ApplicationGUI app, BorderPane globalRoot, MainScene mainScene) {
        this.app = app;
        this.globalRoot = globalRoot;
        this.mainScene = mainScene;
        this.setTop(scrollPane);
        this.setMinWidth(150);
        this.setStyle("-fx-background-color: rgb(40, 40, 40)");
        this.setCenter(statsLayout);
        statsLayout.setPrefWidth(Double.MAX_VALUE);
        generateButtons();
    }

    public void generateStats() {

        statsLayout.getChildren().clear();

        BoxItemList boxes = mainScene.getProblem().getBoxes();
        Container container = mainScene.getProblem().getContainer();

        double spaceEfficiency = (boxes.getVolumeOfPlacedBoxes() / container.getVolume()) * 100;
        spaceEfficiency = Math.round(spaceEfficiency * 100) / 100.0;

        double boxEfficiency = Math.round(boxes.getBoxEfficiency() * 100) / 100.0;

        Label spaceEfficiencyStat = new Label("Bin Fill: " + spaceEfficiency + "%");
        Label boxesPlacedStat = new Label("Boxes Placed: " + boxes.getNumberOfPlacedBoxes() + "/" + boxes.getBoxes().size());
        Label boxesEfficiencyStat = new Label("Box Volume Utilised: " + boxEfficiency + "%");
        
        spaceEfficiencyStat.setTextFill(Color.rgb(245, 245, 245));
        spaceEfficiencyStat.setStyle("-fx-font-weight: bold; -fx-padding: 10 0 10 0; -fx-text-alignment: center");
        spaceEfficiencyStat.setWrapText(true);
        spaceEfficiencyStat.setMaxWidth(Double.MAX_VALUE);
        spaceEfficiencyStat.setAlignment(Pos.CENTER);

        boxesPlacedStat.setTextFill(Color.rgb(245, 245, 245));
        boxesPlacedStat.setStyle("-fx-font-weight: bold; -fx-padding: 10 0 10 0; -fx-text-alignment: center");
        boxesPlacedStat.setWrapText(true);
        boxesPlacedStat.setMaxWidth(Double.MAX_VALUE);
        boxesPlacedStat.setAlignment(Pos.CENTER);

        boxesEfficiencyStat.setTextFill(Color.rgb(245, 245, 245));
        boxesEfficiencyStat.setStyle("-fx-font-weight: bold; -fx-padding: 10 0 10 0; -fx-text-alignment: center");
        boxesEfficiencyStat.setWrapText(true);
        boxesEfficiencyStat.setMaxWidth(Double.MAX_VALUE);
        boxesEfficiencyStat.setAlignment(Pos.CENTER);

        VBox.setVgrow(spaceEfficiencyStat, Priority.ALWAYS);
        VBox.setVgrow(boxesPlacedStat, Priority.ALWAYS);
        VBox.setVgrow(boxesEfficiencyStat, Priority.ALWAYS);
        statsLayout.getChildren().addAll(spaceEfficiencyStat, boxesPlacedStat, boxesEfficiencyStat);
    }

    public void generateButtons() {
        
        buttonLayout.getChildren().clear();
        buttons.clear();

        BoxItemList boxes = mainScene.getProblem().getBoxes();

        // Create new buttons specific to this scene
        for (int i = 0; i < boxes.getBoxes().size(); i++) {

            BoxItem box = boxes.getBoxAt(i);
            ToggleButton button;
            String labelText;

            if (!box.isPlaced()) {
                continue;
            }

            if (box.getName().equals("N/A")) {
                button = new ToggleButton("Hide");
                labelText = "Box " + (i+1);
            }
            else {
                button = new ToggleButton("Hide");
                labelText = box.getName();
            }

            button.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle (final ActionEvent e) {
                    if (button.isSelected()) {
                        box.getBoxComponent().setVisible(false);
                        button.setText("Show");
                    }
                    else {
                        box.getBoxComponent().setVisible(true);
                        button.setText("Hide");
                    }
                }
            });

            buttons.add(button);
            buttonLayout.getChildren().add(addNewRow(button, labelText));
        }

        scrollPane.setContent(buttonLayout);
        scrollPane.setFitToWidth(true);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        buttonLayout.setStyle("-fx-background-color: rgb(40, 40, 40); -fx-padding: 20 0 20 0;");
        buttonLayout.prefHeightProperty().bind(globalRoot.heightProperty().multiply(0.80));

        generateStats();
    }

    private GridPane addNewRow(ToggleButton button, String labelText) {

        GridPane row = new GridPane();
        ColumnConstraints columnConstraint1 = new ColumnConstraints();
        ColumnConstraints columnConstraint2 = new ColumnConstraints();
        columnConstraint1.setPercentWidth(50);
        columnConstraint2.setPercentWidth(50);

        Label label = new Label(labelText);
        label.setTextFill(Color.rgb(245, 245, 245));
        label.setStyle("-fx-font-weight: bold; -fx-padding: 14 0 0 0;");

        GridPane.setRowIndex(label, 0);
        GridPane.setColumnIndex(label, 0);
        GridPane.setHalignment(label, HPos.CENTER);

        GridPane.setRowIndex(button, 0);
        GridPane.setColumnIndex(button, 1);
        GridPane.setHalignment(button, HPos.CENTER);

        GridPane.setHgrow(label, Priority.ALWAYS);
        GridPane.setHgrow(button, Priority.ALWAYS);
        row.setPrefWidth(Double.MAX_VALUE);

        row.getColumnConstraints().addAll(columnConstraint1, columnConstraint2);
        row.getChildren().addAll(label, button);
        return row;
    }
}
