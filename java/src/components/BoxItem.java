package components;

import maths.Coordinate;
import javafx.scene.shape.Box;
import javafx.scene.paint.PhongMaterial;

public class BoxItem {

    private double sizeX;
    private double sizeY;
    private double sizeZ;
    private boolean placed = false;
    private Coordinate coordinates;
    private String name = "N/A";
    private Box boxComponent;
    private PhongMaterial material;

    public BoxItem(double sizeX, double sizeY, double sizeZ) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.sizeZ = sizeZ;
    }

    public BoxItem(String name, double sizeX, double sizeY, double sizeZ) {
        this.name = name;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.sizeZ = sizeZ;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public double getSizeX() {
        return this.sizeX;
    }
    public double getSizeY() {
        return this.sizeY;
    }
    public double getSizeZ() {
        return this.sizeZ;
    }

    public double getVolume() {
        return getSizeX() * getSizeY() * getSizeZ();
    }

    public Coordinate getCoordinates() {
        return this.coordinates;
    }

    public boolean isPlaced() {
        return this.placed;
    }

    public void placeBoxAt(double x, double y, double z) {
        coordinates = new Coordinate(x, y, z);
        placed = true;
    }

    public void placeBoxAt(Coordinate coordinates) {
        this.coordinates = coordinates;
        placed = true;
    }

    public void setMaterial(PhongMaterial material) {
        boxComponent.setMaterial(material);
        this.material = material;
    }

    public PhongMaterial getMaterial() {
        return this.material;
    }

    public Box getBoxComponent() {
        return this.boxComponent;
    }

    public void setBoxComponent(Box boxComponent) {
        boxComponent.setTranslateX(this.getCoordinates().getX());
        boxComponent.setTranslateY(this.getCoordinates().getY());
        boxComponent.setTranslateZ(this.getCoordinates().getZ());
        this.boxComponent = boxComponent;
    }

    @Override
    public String toString() {

        String coord = "";
        if (this.coordinates != null) {
            coord = this.coordinates.toString();
        }

        return "Box Name = '" + this.name + "' | Box Size = (" + this.sizeX + ", " + 
            this.sizeY + ", " + this.sizeZ + ") | Box Location = " + coord + " | Placed? = " +
            this.placed;
    }
}