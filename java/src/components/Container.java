package components;

public class Container {
    
    private double sizeX;
    private double sizeY;
    private double sizeZ;

    public Container(double sizeX, double sizeY, double sizeZ) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.sizeZ = sizeZ;
    }

    public double getSizeX() {
        return this.sizeX;
    }
    public double getSizeY() {
        return this.sizeY;
    }
    public double getSizeZ() {
        return this.sizeZ;
    }

    public double getVolume() {
        return this.sizeX * this.sizeY * this.sizeZ;
    }

    @Override
    public String toString() {
        return "Container Size = (" + this.getSizeX() + ", " + this.getSizeY() + 
        ", " + this.getSizeZ() + ")";
    }
}