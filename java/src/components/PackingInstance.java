package components;

import io.DataFile;
import java.io.File;

public class PackingInstance {
    
    private BoxItemList boxes;
    private Container container;

    public void setBoxes(BoxItemList boxes) {
        this.boxes = boxes;
    }

    public void setContainer(Container container) {
        this.container = container;
    }

    public BoxItemList getBoxes() {
        return this.boxes;
    }

    public Container getContainer() {
        return this.container;
    }

    public static PackingInstance generateRandomInstance(int numberOfBoxes, int minSize, int maxSize, int binX, int binY, int binZ) {
        PackingInstance p = new PackingInstance();
        p.setBoxes(new BoxItemList());
        p.getBoxes().generateRandomBoxes(numberOfBoxes, minSize, maxSize, minSize, maxSize, minSize, maxSize);
        p.setContainer(new Container(binX, binY, binZ));
        return p;
    }
}
