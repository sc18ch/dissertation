package components;

import java.util.*;

public class BoxItemList {

    private List<BoxItem> boxes = new ArrayList<BoxItem>();

    public BoxItemList(){}

    public BoxItemList(ArrayList<ArrayList<Integer>> boxSizes) {
        for (ArrayList<Integer> boxSize : boxSizes) {
            this.addBox(new BoxItem(boxSize.get(0), boxSize.get(1), boxSize.get(2)));
        }
    }

    public void generateRandomBoxes(int amount, double minSizeX, double maxSizeX, double minSizeY, double maxSizeY, double minSizeZ, double maxSizeZ) {
        
        Random r = new Random();
        for (int i = 0; i < amount; i++) {
            double x = Math.round((minSizeX + (maxSizeX - minSizeX) * r.nextDouble()) * 100.0) / 100.0;
            double y = Math.round((minSizeY + (maxSizeY - minSizeY) * r.nextDouble()) * 100.0) / 100.0;
            double z = Math.round((minSizeZ + (maxSizeZ - minSizeZ) * r.nextDouble()) * 100.0) / 100.0;

            this.boxes.add(new BoxItem(x, y, z));
        }
    }

    public void generateUniformBoxes(int amount, double sizeX, double sizeY, double sizeZ) {
        
        for (int i = 0; i < amount; i++) {
            this.boxes.add(new BoxItem(sizeX, sizeY, sizeZ));
        }
    }

    @Override
    public String toString() {
        String s = "";
        for (BoxItem box : this.boxes) {
            s += box.toString() + "\n";
        }
        return s;
    }

    public void addBox(BoxItem box) {
        this.boxes.add(box);
    }

    public List<BoxItem> getBoxes() {
        return boxes;
    }

    public BoxItem getBoxAt(int index) {
        return this.boxes.get(index);
    }

    public int getNumberOfPlacedBoxes() {

        int numOfBoxes = 0;
        for (BoxItem box : this.boxes) {
            if (box.isPlaced()) {
                numOfBoxes += 1;
            }
        }

        return numOfBoxes;
    }

    public double getBoxEfficiency() {
        return (getVolumeOfPlacedBoxes() / getVolumeOfAllBoxes()) * 100;
    }

    public double getVolumeOfAllBoxes() {

        double totalVolume = 0;
        for (BoxItem box : this.boxes) {
            totalVolume += box.getVolume();
        }

        return totalVolume;
    }

    public double getVolumeOfPlacedBoxes() {

        double totalVolume = 0;
        for (BoxItem box : this.boxes) {
            if (box.isPlaced()) {
                totalVolume += box.getVolume();
            }
        }

        return totalVolume;
    }

    public BoxItem getLargestVolumeBox() {
        BoxItem largestVolumeBox = null;
        for (BoxItem box : this.boxes) {
            if (largestVolumeBox != null) {
                if (largestVolumeBox.getVolume() < box.getVolume()) {
                    largestVolumeBox = box;
                }
            }
            else {
                largestVolumeBox = box;
            }
        }
        return largestVolumeBox;
    }
}