package main;

import javafx.application.Application;
import gui.ApplicationGUI;

public class Main {

    public static void main(String[] args) {
        Application.launch(ApplicationGUI.class, args);
    }
}