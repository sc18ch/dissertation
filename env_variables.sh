#!/bin/sh
echo ">>> Setting up env variables..."
export GUROBI_HOME="${PWD}/lib/gurobi951/linux64"
echo ">>> Set GUROBI_HOME='${GUROBI_HOME}'"
export PATH="${PATH}:${GUROBI_HOME}/bin"
echo ">>> Added '${GUROBI_HOME}/bin' to PATH"
export PATH="${PATH}:${PWD}/lib/minizinc/bin"
echo ">>> Added '${PWD}/lib/minizinc/bin' to PATH"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${GUROBI_HOME}/lib"
echo ">>> Set LD_LIBRARY_PATH='${LD_LIBRARY_PATH}'"
export GRB_LICENSE_FILE="${GUROBI_HOME}/gurobi.lic"
echo ">>> Set GRB_LICENSE_FILE='${GRB_LICENSE_FILE}'"
echo ">>> DONE!"
