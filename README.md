# 3D-KP Final Year Project
This repository contains the following:
- Visualisation application, located in `java`
- MiniZinc implementations, located in `solver`
- Report, located in `report`

The `lib` folder contains the libraries MiniZinc and Gurobi, which are used by the solver.

The programs and code in this repository are only compatible with Linux. This project was developed specificallly on Ubuntu 20.04.4 LTS.

## Running the Java visualisation application
### Setup
To setup dependencies, run the `setup.sh` script using `. ./setup.sh` in this directory. This project uses OpenJDK 17.0.2, so the setup script will ensure this is installed, along with setting environment variables to use in the terminal session.

To individually setup the environment variables, run the `env_variables.sh` script using `. ./env_variables.sh` (NOTE: This is already executed in `setup.sh`).

### Building
To build the project, run the `make.sh` script using `. ./make.sh` in this directory.

### Running
Once the `setup.sh` and `build.sh` scripts have completed successfully, the visualisation application can be launched using the `run.sh` script. This can be launched using `. ./run.sh` in this directory.

Note that the application loads an initial "test scene" - this can be used to familiarise yourself with the controls of the application.

### Camera controls
The 3D camera can be controlled in the following ways:
- Right-click and dragging - this rotates the scene 
- Middle-mouse-click and dragging - this pans the camera
- Keys **W** and **S** - used to zoom in and out of the scene

## Running the MiniZinc models
### Setup
Firstly, the `setup.sh` or `env_variables.sh` scripts must have been previously run in the terminal session you wish to run the MiniZinc models.

Secondly, the Gurobi licence located at `lib/gurobi951/linux64/gurobi.lic` is only valid for a single machine, therefore it will not allow you to use Gurobi until you have updated this licence with one of yours. Note that everything was done to avoid this step and provide a universal licence, however Gurobi simply does not offer licences which aren't tied to single machines. Fortunately, Gurobi provides academic licences for free with relative ease. 

Follow the steps below to gain access to a licence:
- Go to `https://www.gurobi.com/`
- Click **Login**
- Login or Register for an account
- Once successfully logged in, navigate to `https://www.gurobi.com/downloads/end-user-license-agreement-academic/`
- Click **I Accept These Conditions**
- Scroll down to **Installation** on the licence page, and copy the `grbgetkey <KEY_HERE>` command
- Navigate back to the terminal which has previously run `setup.sh` or `env_variables.sh` and paste the command into the terminal, press enter
- The terminal should display **In which directory would you like to store the Gurobi license file?**. Paste the directory `./lib/gurobi951/linux64/` into the terminal and hit enter
- The terminal should state that **License ### written to file /..../lib/gurobi951/linux64/gurobi.lic**
- From this point, the licence should be set up and can be used until it expires

### Running
Once the setup is complete and a licence has been created (ensuring either `setup.sh` or `env_variables.sh` has been run in this terminal window), type the command `cd solver/` to navigate to the **solver** folder.

From this folder, the following scripts are available:
- `run_solver_input.sh` - running this script will prompt input for the name of a MiniZinc model file (.mzn). These are located in `solver/models/knapsack/`. The script will then prompt for a MiniZinc instance file (.dzn). These are located in `solver/instances/knapsack/`. Finally, the script will ask for a name for the output (.log) file, which is then saved in `solver/logs/`. **Note** that neither the path or the file extension (e.g. .mzn) needs to be specified in the input. For example if the CP model was desired to be run on the full fit 4 boxes instance, then the first input of the script would be `knapsack_CP_3D` and the second input would be `full_fit_4_boxes_3D`.
- `run_solver_CP.sh` - this script will run the CP model on the instance hard coded within the script itself. To change what instance is run, the script must be modified.
- `run_solver_ILP.sh` - this script will run the ILP model on the instance hard coded within the script itself. To change what instance is run, the script must be modified.
- `run_solver_Silva.sh` - this script will run the S model on the instance hard coded within the script itself. To change what instance is run, the script must be modified.

## Visualising a MiniZinc output
Once a MiniZinc `.log` file has been generated for instance **X**, launch the visualisation application and perform the following:
- Go to the **File** menu on the menu bar at the top and select **Open LP Output File**
- You will be prompted to first select an **Instance File** (.dzn). Select the instance file for the instance the log was outputted for (instance **X**)
- After a successful loading, the next file prompt will be for the output file (.log). Navigate to `solver/logs/` and select the output file you wish to load
- The load should be successful, and the solution will be displayed on screen. You can now use the camera controls defined previously to navigate the scene
