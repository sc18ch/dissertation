#!/bin/sh
echo ">>> Setting up requirements..."
sudo apt-get update  # To get the latest package lists
sudo apt-get install openjdk-17-jre-headless -y
sudo apt-get install openjfx
source env_variables.sh
echo ">>> SETUP COMPLETE!"
