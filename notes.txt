< TODO List >

    --> REPORT
        - <DRAFT1 DONE>	Finish prelude + summary draft (take care on whether project is knapsack or bpp)
        - <WIP>	Finish chapter 1 draft
        - <WIP>	Finish chapter 2 draft
        - <DONE>	Run tests and records results for instances against ILP, LI and Silva
        - Finish chapter 3 draft
        - Finish chapter 4 draft
        - Finish appendix draft

    --> VISUALISATION
        - Add some way to see non-placed boxes in UI

< NOTES FOR REPORT >

	- Change order of box input to see if it affects the solver (Gurobi doesn't necessarily use an EXACT method for solving, could be slightly heuristic based

< Paste Saves >



